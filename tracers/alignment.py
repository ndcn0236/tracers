"""
Align compartments with each other
"""
from . import utils
import scipy as sp
from .brain import Compartment


def align_compartments(moving, reference):
    """
    Aligns two compartments with each other

    :param moving: Compartment that will be shifted to align with reference
    :type moving: Compartment
    :param reference: Compartment that will be kept stationary
    :type reference: Compartment
    :return: number of steps to move the moving compartment downwards (i.e. to add to the index of every slice)
    """
    if moving.brain is not reference.brain:
        raise ValueError("Compartments are not part of the same brain")
    shift = list(sp.arange(-len(moving) + 1, len(reference)))
    covariance = sp.zeros(len(shift))
    for idx_ref, ref_slice in enumerate(reference):
        for mesh in moving.brain.meshes.values():
            if mesh.align:
                for idx_moving, moving_slice in enumerate(moving):
                    if ref_slice in mesh and moving_slice in mesh:
                        ref_outline = mesh.from_slice(ref_slice)
                        mov_outline = mesh.from_slice(moving_slice)
                        idx_shift = shift.index(idx_ref - idx_moving)
                        covariance[idx_shift] += align_outlines(ref_outline.points, mov_outline.points,
                                                                nbreak=10, npoints=50)[1]
    if (covariance == 0).all():
        raise ValueError("No meshes selected for alignment")
    return shift[sp.argmax(covariance)]


def align_outlines(outline1, outline2, nbreak=50, npoints=300):
    """
    Aligns two outlines

    :param outline1: (N, 2) array of points
    :param outline2: (M, 2) array of points
    :param nbreak: How many uniformly distributed break-points to try
    :param npoints: number of points to resample the longer contour to
    :return: Returns the best-fit alignment and the quality of that alignment:
    - tuple with matching points on the two surfaces ((s1, r1), (s2, r2))
    where the point distance s1 from the first point on outline1 is equivalent to the point s2 from the first point on outline2
    same for point r1 and r2
    - float with the quality of the alignment (higher value = better alignment)
    """
    length1, length2 = utils.length(outline1), utils.length(outline2)
    if length1 > length2:
        reference = outline1
        moving = outline2
        lref, lmoving = length1, length2
    else:
        reference = outline2
        moving = outline1
        lref, lmoving = length2, length1

    if not utils.is_closed(moving):
        # shorter open outline is a subset of the longer closed or open outline
        offset, corr = covariance(reference, moving, resample=lref / npoints)
        max_cov = corr.max()
        best_offset = offset[sp.argmax(corr)]
        if (best_offset + lmoving) > lref and not utils.is_closed(reference):
            best_offset = lref - lmoving
        equivalent = ((0, lmoving), (best_offset, best_offset + lmoving))
    else:
        # shorter closed outline is a subset of the longer closed or open outline
        # need to break the closed moving outline at the correct point to match the reference outline
        max_cov = -sp.inf
        if utils.is_closed(reference):
            # test if the 2 surfaces might be equivalent after all
            moving_long = moving * lref / lmoving
            offset, corr = covariance(reference, moving_long, resample=lref / npoints)
            if corr.max() > max_cov:
                max_cov = corr.max()
                best_offset = offset[sp.argmax(corr)]
                equivalent = ((0, lmoving), (best_offset, best_offset + lref))
        if not utils.is_closed(reference) or lref / lmoving > 1.5:
            moving_resampled = utils.resample_contour(moving, npoints=nbreak * 10 + 1, double=True)
            for breakpoint in sp.arange(nbreak):
                offset, cov = covariance(reference, moving_resampled[breakpoint * 10: (breakpoint + nbreak) * 10], lref / npoints)
                if cov.max() > max_cov:
                    max_cov = cov.max()
                    max_breakpoint = breakpoint * lmoving / nbreak
                    max_offset = offset[cov.argmax()]
                    if not utils.is_closed(reference) and (max_offset + lmoving) > lref:
                        max_offset = lref - lmoving
                    equivalent = ((max_breakpoint, max_breakpoint + lmoving), (max_offset, max_offset + lmoving))
    if length1 > length2:
        equivalent = (equivalent[1], equivalent[0])
    return equivalent, max_cov


def covariance(reference, moving, resample):
    """
    Computes the correlation matrix between two outlines in the same mesh

    :param reference: (N, 2) array of points
    :param moving: (M, 2) array of points
    :param resample: distance between points after resampling
    :return: offset, covariance array
    """
    ref_points = utils.resample_contour(reference, offset=resample, double=True)
    ref_normal = utils.normal(ref_points)
    mov_points = utils.resample_contour(moving, offset=resample, double=False)
    mov_normal = utils.normal(mov_points)
    corr = sp.sum([sp.correlate(mov_normal[:, dim], ref_normal[:, dim], 'valid') for dim in range(2)], 0)[::-1]
    offset = sp.arange(corr.size) * resample
    return offset, corr * resample

