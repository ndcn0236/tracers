import scipy as sp
from scipy import ndimage
from scipy.interpolate import interp1d
import numba
from operator import xor
import re
from matplotlib import colors


def is_closed(contour):
    """
    Tests whether a contour is closed

    :param contour: (N, 2) array of points along an outline
    :return: True if the first and last point are within 1e-4 of each other
    """
    return ((contour[0] - contour[-1]) ** 2).sum() < 1e-8


def length(contour):
    """
    Computes the length of a contour

    :param contour: (N, 2) array of points along an outline
    :return: length of the outline
    """
    return sp.sqrt(((contour[1:] - contour[:-1]) ** 2).sum(1)).sum()


def resample_contour(contour, npoints=None, offset=None, double=False):
    """
    Resamples along a surface

    :param contour: (N, 2) array of points along an outline
    :param npoints: number of new points M
    :param offset: distance between neighbouring points
    :param double: if True doubles closed contours
    :return: (M, 2) array of new points
    """
    if not xor(npoints is None, offset is None):
        raise ValueError('Either npoints or offset should be set, not both')
    dist = sp.sqrt(sp.sum((contour[1:] - contour[:-1]) ** 2, -1))
    sum_dist = sp.append(0, sp.cumsum(dist))
    use = sp.append(True, dist != 0)
    if npoints is None:
        npoints = max(int(round(sum_dist[-1] / offset)), 3 if is_closed(contour) else 2)
    res = interp1d(sum_dist[use], contour[use], axis=0)(sp.linspace(0, sum_dist[-1], npoints))
    if double and is_closed(contour):
        res = sp.append(res, res[1:], 0)
    return res


def interpolate(contour, dist):
    """
    Computes the points `dist` away from the starting point of the contour

    :param contour: (N, 2) array of points along an outline
    :param dist: (K, ) array of distances from the starting point
    :return: (K, 2) array of points
    """
    l = length(contour)
    if is_closed(contour):
        dist = dist % l
    elif (dist < 0).any() or (dist > l).any():
        raise ValueError("distance can not be greater than length for open contour")
    dstep = sp.sqrt(((contour[1:] - contour[:-1]) ** 2).sum(1))
    dseq = sp.cumsum(dstep)
    idx = sp.digitize(dist, dseq)
    idx[idx == dstep.size] = dstep.size - 1
    doff = ((dist - sp.append(0, dseq)[idx]) / dstep[idx])[:, None]
    lower = contour[idx]
    upper = contour[idx + 1]
    return doff * upper + (1 - doff) * lower


def normal(contour):
    """
    Compute the normal from the contour

    :param contour: (N, 2) array with points along an outline
    :return: (N, 2) array with the normals estimated at the points
    """
    dx = contour[1:, 0] - contour[:-1, 0]
    dy = contour[1:, 1] - contour[:-1, 1]
    norm_lines = sp.stack((-dy, dx), -1)
    norm_lines /= sp.sqrt(sp.sum(norm_lines ** 2, -1))[:, None]
    res = sp.zeros(contour.shape)
    res[1:] += norm_lines
    res[:-1] += norm_lines
    res /= sp.sqrt(sp.sum(res ** 2, -1))[:, None]
    return res

def outward_normal(contour):
    """
    Returns a flipped contour if the normals point inwards

    :param contour: (N, 2) array with points along an outline
    :return: (N, 2) array with the normals flipped if they point inwards
    """
    length = sp.sqrt(sp.sum((contour[1:] - contour[:-1]) ** 2, -1))
    weight = sp.zeros(contour.shape[0])
    weight[1:] += length / 2.
    weight[:-1] += length / 2.

    def weighted_var(pos):
        mean_pos = (weight[:, None] * pos).sum(0) / weight.sum()
        return (weight[:, None] * (pos - mean_pos) ** 2).sum() / weight.sum()
    if weighted_var(contour) > weighted_var(contour + normal(contour)):
        return contour[::-1]
    else:
        return contour


def curvature(contour, nsmooth=3):
    """
    Compute the (signed) curvature from the contour

    :param contour: (N, 2) array with points along an outline
    :param nsmooth: size of the Gaussian smoothing of the curvature
    :return: (N, ) array with the smoothed curvature
    """
    second_der = contour[2:] + contour[:-2] - 2 * contour[1:-1]
    curv = sp.sqrt((second_der ** 2).sum(-1))
    alignment = (normal(contour)[1:-1] * second_der).sum(-1) > 0
    curv[~alignment] *= -1
    return ndimage.gaussian_filter(curv, nsmooth, mode='reflect')


def plane_outline(vertices, triangles, position=(0, 0, 0), orientation=(0, 0, 1)):
    """Returns list of ProjectedMesh of the surface projected onto a plane.

    :param vertices: (N, 3) array of control points
    :param triangles: (M, 3) int array of indices connecting the control points with triangles
    :param position: origin of the plane on which the contour will be computed
    :param orientation:  normal of the plane on which the contour will be computed
    :return: list of (K, 3) array which forms the outline of the mesh on the plane
    """
    position = sp.asarray(position)
    orientation = sp.asarray(orientation)
    assert position.size == 3, "3-dimensional position required"
    assert orientation.size == 3, "3-dimensional orientation required"
    norm_orient = orientation / sp.sqrt(sp.sum(orientation ** 2))

    relative_offset = sp.sum((vertices - position[None, :]) * norm_orient[None, :], 1)

    vertex_above_plane = (relative_offset > 0)[triangles]
    total_above_plane = sp.sum(vertex_above_plane, 1)
    lonely_point = -sp.ones(total_above_plane.size, dtype='i4')
    lonely_point[total_above_plane == 1] = sp.where(vertex_above_plane[total_above_plane == 1, :])[1]
    lonely_point[total_above_plane == 2] = sp.where(~vertex_above_plane[total_above_plane == 2, :])[1]
    other_point1 = (lonely_point + 1) % 3
    other_point2 = (lonely_point + 2) % 3
    lines = triangles[sp.arange(lonely_point.size), [[lonely_point, other_point1],
                                                     [lonely_point, other_point2]]]
    lines[:, :, lonely_point == -1] = -1  # (2 lines, 2 points, N vertices)
    lines = sp.sort(lines, 1)

    routes = []
    tmp_route = sp.zeros((sp.sum(lonely_point != -1) * 2 + 1, 3), dtype='i8')
    while (lines != -1).any():
        idx_min, idx_max = _trace_route(lines, tmp_route)
        indices = tmp_route[idx_min:idx_max, :]
        vertex = indices[:, 0]
        points = indices[:, 1:]

        norm_orient = orientation / sp.sqrt(sp.sum(orientation ** 2))
        relative_offset = sp.sum((vertices - position[None, :]) * norm_orient[None, :], 1)
        offset = abs(relative_offset[points])
        pos = vertices[points, :]
        routes.append(sp.sum(offset[:, ::-1, None] * pos, 1) / sp.sum(offset, 1)[:, None])

    return routes


@numba.jit(nopython=True)
def _trace_route(lines, output_arr):
    """Helper function to compute the slice through a mesh
    """
    idx_out_start = output_arr.shape[0] // 2
    for idx_vertex in range(lines.shape[2]):
        if lines[0, 0, idx_vertex] != -1:
            idx_start = idx_vertex
            break
    output_arr[idx_out_start, 0] = idx_vertex
    output_arr[idx_out_start, 1] = lines[0, 0, idx_vertex]
    output_arr[idx_out_start, 2] = lines[0, 1, idx_vertex]
    circle_found = False
    idx_min = idx_out_start
    idx_max = idx_out_start
    for idx_line, direction_out in enumerate((1, -1)):
        found_next = True
        idx_cvert = idx_start
        idx_cline = idx_line
        idx_out = idx_out_start
        while found_next:
            idx_out += direction_out
            found_next = False
            for idx_vertex in range(lines.shape[2]):
                if idx_vertex != idx_cvert:
                    for idx_line in range(2):
                        if (lines[idx_cline, 0, idx_cvert] == lines[idx_line, 0, idx_vertex] and
                                    lines[idx_cline, 1, idx_cvert] == lines[idx_line, 1, idx_vertex]):
                            found_next = True
                            break
                    if found_next:
                        break
            if not found_next:
                if direction_out == 1:
                    idx_max = idx_out
                else:
                    idx_min = idx_out + 1
                break
            output_arr[idx_out, 0] = idx_vertex
            if idx_start == idx_vertex:
                circle_found = True
                output_arr[idx_out, 1] = output_arr[idx_out_start, 1]
                output_arr[idx_out, 2] = output_arr[idx_out_start, 2]
                lines[:, :, idx_cvert] = -1
                break
            else:
                idx_cline = 1 - idx_line
                if direction_out == 1:
                    output_arr[idx_out, 1] = lines[idx_cline, 0, idx_vertex]
                    output_arr[idx_out, 2] = lines[idx_cline, 1, idx_vertex]
                else:
                    output_arr[idx_out, 1] = lines[idx_line, 0, idx_vertex]
                    output_arr[idx_out, 2] = lines[idx_line, 1, idx_vertex]
                if idx_cvert != idx_start:
                    lines[:, :, idx_cvert] = -1
                idx_cvert = idx_vertex
        if circle_found:
            idx_max = idx_out + 1
            break
    lines[:, :, idx_start] = -1
    return idx_min, idx_max


def sort_filenames(filenames):
    if len(filenames) == 1:
        return filenames
    start = 0
    while all(filename[:start] == filenames[0][:start] for filename in filenames):
        start += 1
    start -= 1
    end = -1
    while all(filename[end:] == filenames[0][end:] for filename in filenames):
        end -= 1
    end += 1
    if end == 0:
        end = None
    interesting_parts = [filename[start:end] for filename in filenames]
    digits = [sum(c.isdigit() for c in s) for s in interesting_parts]
    extended = ['0' * (max(digits) - d) + s for s, d in zip(interesting_parts, digits)]
    print(extended)
    return [filenames[idx] for idx in sp.argsort(extended)]


translations = {'brightgray': 'lightgray',
                'cream': 'Moccasin'}


def to_mpl_color(name):
    found = re.search('RGB \(([0-9]*), ([0-9]*), ([0-9]*)\)', name)
    if found:
        return tuple(int(color) / 255. for color in found.groups())
    name = translations.get(name.lower(), name.lower())
    try:
        colors.to_rgb(name)
    except KeyError:
        return 'black'
    return name


