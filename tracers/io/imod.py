import scipy as sp
from .. import utils


property_type = {'contour': (int, ) * 3,
                 'imod': (int, ),
                 'mesh': (int, ) * 3,
                 'object': (int, ) * 3}


class ImodObject(object):
    def __init__(self, contours, meshes, **props):
        self.contours = contours
        self.meshes = meshes
        for name, value in props.items():
            setattr(self, name, value)

    def slice(self, zval, only_longest=False):
        """Finds the outlines at a specific z-value.

        :param zval: z-value where the outlines are computed
        :param only_longest: only return the longest contour
        :return: list of (K, 2) arrays with the x-, y-coordinates of the outline on the brain (list of them if `only_longest` is False)
        """
        if len(self.meshes) != 0:
            all_contours = []
            for mesh in self.meshes:
                all_contours.extend(utils.plane_outline(mesh[0][:, 0, :], mesh[1],
                                                        (0., 0., zval), (0., 0., 1.)))
            if only_longest:
                length = [sp.sqrt(sp.sum((loc[1:] - loc[:-1]) ** 2, -1)).sum() for loc in all_contours]
                return all_contours[sp.argmin(length)]
            return all_contours
        else:
            idx = sp.argmin([abs(zval - contour[:, 2].mean()) for contour in self.contours])
            return self.contours[idx] if only_longest else [self.contours[idx]]

    @property
    def bounding_box(self, ):
        """
        The bounding box includes all the meshes/contours

        :return: nested tuple with ((xmin, xmax), (ymin, ymax), (zmin, zmax))
        """
        res = sp.zeros((3, 2))
        res[:, 0] = sp.inf
        res[:, 1] = -sp.inf
        for dim in range(3):
            for mesh in self.meshes:
                res[dim, 0] = min(res[dim, 0], mesh[0][:, 0, dim].min())
                res[dim, 1] = max(res[dim, 1], mesh[0][:, 0, dim].max())
            for contour_dict in self.contours:
                for contour in contour_dict.values():
                    res[dim, 0] = min(res[dim, 0], contour[:, dim].min())
                    res[dim, 1] = max(res[dim, 1], contour[:, dim].max())
        return tuple(tuple(arr) for arr in res)


def read_imod_line(line, in_mesh=False, in_contour=False, in_view=False):
    if in_mesh and in_contour:
        raise ValueError("Can't be in both a mesh and contour")
    if (len(line.split()) == 0 or line[0] == '#'):  # empty line or comment
        return None, in_mesh, in_contour, in_view
    if in_contour:
        if len(line.split()) != 3:
            return read_imod_line(line, in_mesh, False)
        try:
            return tuple(float(value) for value in line.split()), in_mesh, in_contour, False
        except ValueError:
            return read_imod_line(line, in_mesh, False)
    if in_mesh:
        if len(line.split()) == 3:
            return tuple(float(value) for value in line.split()), in_mesh, in_contour, False
        value = (int(line), )
        if value[0] == -1:
            return None, False, in_contour, False
        else:
            return value, in_mesh, in_contour, False

    property_name = line.split()[0]
    if property_name in property_type:
        if len(line.split()) != len(property_type[property_name]) + 1:
            raise ValueError('Expected %i values for %s, not %s' % (len(property_type[property_name]), property_name,
                                                                    line))
        value = tuple((tp(elem) for tp, elem in zip(property_type[property_name], line.split()[1:])))
    else:
        value = line[len(property_name):].strip()
    return (property_name, value), property_name == 'mesh', property_name == 'contour', property_name == 'view' or in_view


def read_imod(filename):
    objects = None
    props = {}
    current_object = None
    current_triangle = []
    with open(filename, 'r') as f:
        in_mesh, in_contour, in_view = False, False, False
        for line in f.readlines():
            data, in_mesh, in_contour, in_view = read_imod_line(line, in_mesh, in_contour, in_view)
            if data is None:
                pass
            elif data[0] == 'imod':
                if objects is not None:
                    raise IOError('Expected only one line with "imod"')
                objects = sp.zeros(data[1], dtype='object')
            elif data[0] == 'object':
                current_object = ImodObject(sp.zeros(data[1][1], dtype='object'),
                                            sp.zeros(data[1][2], dtype='object'))
                current_object.contours[()] = ({}, ) * data[1][1]
                objects[data[1][0]] = current_object
                in_view = False
            elif data[0] == 'contour':
                current_contour = sp.zeros((data[1][2], 3))
                current_object.contours[data[1][0]][data[1][1]] = current_contour
                idx = 0
                in_view = False
            elif data[0] == 'mesh':
                current_mesh = ([], [])
                current_object.meshes[data[1][0]] = current_mesh
                nvertex = data[1][1]
                nindex = data[1][2]
                idx = 0
                in_view = False
            else:
                if in_view:
                    pass
                elif in_mesh:
                    if idx < nvertex:
                        if idx % 2 == 0:
                            vertex = data
                        else:
                            current_mesh[0].append((vertex, data))
                    elif idx < nvertex + nindex - 1:
                        if len(data) != 1:
                            raise ValueError('only expected single number for mesh index')
                        if data[0] == -22:
                            if len(current_triangle) != 0:
                                raise IOError('Did not finish last triangle')
                        elif data[0] == -25:
                            if len(current_triangle) != 0:
                                raise IOError('Did not finish last triangle of previous mesh')
                        elif data[0] < 0:
                            raise NotImplementedError('No support for mesh index %i' % data[0])
                        else:
                            current_triangle.append(data[0] / 2)
                            if len(current_triangle) == 3:
                                current_mesh[1].append(sp.array(current_triangle))
                                current_triangle = []
                    else:
                        raise ValueError('Still in mesh even though the mesh is finished')
                    idx += 1
                elif in_contour:
                    current_contour[idx] = data
                    idx += 1
                elif data[0] in ('contflags', 'conttime', 'Meshflags', 'Meshsurf', 'Meshtime',
                                 'Meshflags', 'Meshsurf', 'Meshtime'):
                    pass
                elif current_object is None:
                    if data[0] in props:
                        raise ValueError('Overriding property %s' % data[0])
                    props[data[0]] = data[1]
                else:
                    if hasattr(current_object, data[0]):
                        raise ValueError('Overriding property %s' % data[0])
                    setattr(current_object, data[0], data[1])

    for object in objects:
        object.meshes = tuple((sp.array(vert, dtype='float'),
                               sp.array(ind, dtype='int')) for vert, ind in object.meshes)
    return {object.name: object for object in objects}, props


