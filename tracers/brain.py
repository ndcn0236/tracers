from collections import MutableSequence, Set, MutableSet, OrderedDict
import json
import scipy as sp
from scipy import stats
import os
from . import utils


class Brain(object):
    """
    Collection of all the outlines drawn for a specific monkey brain
    """
    possible_species = {'MF', 'MN', 'MR'}

    def __init__(self, identifier, species, ncompartments=24, dist_compartments=0.04):
        self.identifier = identifier
        self.species = species
        self.compartments = ()
        self.ncompartments = ncompartments
        self.dist_compartments = dist_compartments
        self.meshes = OrderedDict([('WM/GM boundary', Mesh(self)),
                                   ('Pial surface', Mesh(self))])

    @property
    def ncompartments(self, ):
        return len(self.compartments)

    @ncompartments.setter
    def ncompartments(self, value):
        if self.ncompartments < value:
            self.compartments = self.compartments + tuple(Compartment(self, ) for _ in
                                                          range(value - self.ncompartments))
        else:
            self.compartments = self.compartments[:value]
        if self.ncompartments != value:
            raise ValueError("Failed to create the correct number of compartments")

    @property
    def shape(self, ):
        return (sum(not comp.empty() for comp in self), max(len(comp) for comp in self))

    def __getitem__(self, id_compartment):
        """
        Retrieves a compartment with the given integer index

        :param id_compartment: integer index
        :return:
        """
        return self.compartments[id_compartment - 1]

    def __len__(self, ):
        return self.ncompartments

    def __contains__(self, item):
        return item in self.compartments

    def __iter__(self):
        for comp in self.compartments:
            yield comp

    @property
    def species(self, ):
        return self._species

    @species.setter
    def species(self, value):
        if value not in self.possible_species:
            raise ValueError("Species %s not recognized" % value)
        self._species = value

    def __str__(self, ):
        return '%s(%s)' % (self.identifier, self.species)

    def __repr__(self, ):
        return '%s(%r, %r)' % (self.__class__.__name__, self.identifier, self.species)

    def empty_compartments(self, ):
        for comp in self:
            if comp.empty():
                yield comp

    def real_compartments(self, ):
        for comp in self:
            if not comp.empty():
                yield comp

    def slices(self, ):
        """
        Iterates over all non-empty slices

        :return:
        """
        for comp in self.real_compartments():
            for slice in comp:
                if len(slice) != 0:
                    yield slice

    def save(self, filename):
        """
        Saves the brain data to a JSON file

        :param filename: filename ending with .json
        """
        with open(filename, 'w') as f:
            json.dump(self.to_json_object(), f)

    def export(self, directory):
        """
        Saves the brain data to a series of ASCII files

        :param directory: parent directory of the output
        """
        for compartment in self.real_compartments():
            print('writing %s%s_S???_C%03i_%s.ASC' %
                  (self.species, self.identifier,
                   compartment.id_compartment, '_'.join(compartment.stainings)))
            for slice in compartment:
                if len(slice) != 0:
                    filename = os.path.join(directory, '%s%s_S%03i_C%03i_%s.ASC' % (
                        self.species, self.identifier,
                        slice.in_compartment, compartment.id_compartment,
                        '_'.join(slice.stainings)))
                    slice.to_filename(filename)

    @classmethod
    def load(cls, filename):
        with open(filename, 'r') as f:
            return cls.from_json_object(json.load(f))

    def to_json_object(self, ):
        """
        Returns a set of basic objects that can be stored in a json file

        :return: dictionary containing all the outlines
        """
        res = {}
        res['compartments'] = {comp.id_compartment: comp.to_json_object() for comp in self.real_compartments()}
        res['ncompartments'] = self.ncompartments
        res['dist_compartments'] = self.dist_compartments
        res['species'] = self.species
        res['identifier'] = self.identifier
        res['meshes'] = {name: mesh.to_json_object() for name, mesh in self.meshes.items()}
        return res

    @classmethod
    def from_json_object(cls, dict_obj):
        res = cls(dict_obj['identifier'], dict_obj['species'], dict_obj['ncompartments'], dict_obj['dist_compartments'])
        for idx, json_obj in dict_obj['compartments'].items():
            res[int(idx)].from_json_object(json_obj)
        if 'meshes' in dict_obj:
            for name, json_obj in dict_obj['meshes'].items():
                res.meshes[name] = Mesh.from_json_object(res, json_obj)
        return res

    def align_compartments(self, reference=None):
        from . import alignment
        for compartment in self:
            while len(compartment) != 0 and len(compartment[0]) == 0:
                del compartment[0]
        if reference is None:
            reference = self[sp.argmax([len(c) for c in self.compartments]) + 1]
        shift_compartments = sp.zeros(len(list(self.real_compartments())), dtype='i4')
        for idx, compartment in enumerate(self.real_compartments()):
            if len(compartment) > 0 and compartment is not reference:
                shift_compartments[idx] = alignment.align_compartments(compartment, reference)
        shift_compartments += shift_compartments.min()
        for shift, compartment in zip(shift_compartments, self.real_compartments()):
            if len(compartment) != 0:
                for _ in range(shift):
                    compartment.insert(0, ())

    def register(self, ):
        from . import registration
        rc = registration.RegistrationCost(self)
        offset, angle = rc.best_rigid()
        for slice, o, a in zip(self.slices(), offset, angle):
            slice.register = (tuple(o), a)


class Compartment(MutableSequence):
    """
    Regularly spaced outlines which all have the same staining
    """
    def __init__(self, brain, stainings=None, slices=None):
        """
        Defines a new compartment

        :param brain: collection of all compartments forming a brain
        :type brain: Brain
        :param stainings: list of stainings used on this slice
        :param slices: list of slices used
        """
        self.brain = brain
        if stainings is None:
            stainings = ()
        self.stainings = StainingList(stainings)
        if slices is None:
            slices = []
        self.slices = list(slices)

    def empty(self, ):
        return len(self) == 0 and (self.stainings is None or len(self.stainings) == 0)

    def __len__(self, ):
        return len(self.slices)

    @property
    def id_compartment(self):
        return self.brain.compartments.index(self) + 1

    @id_compartment.setter
    def id_compartment(self, value):
        """
        Checks whether the new id_compartment is valid and that the compartment does not exist already

        :param value: new integer id of the compartment
        """
        if value == self.id_compartment:
            return
        old_id = self.id_compartment
        replace_compartment = self.brain[value]
        if not replace_compartment.empty():
            raise ValueError("Compartment with id %i already exists" % value)
        compartment_list = list(self.brain.compartments)
        compartment_list[self.id_compartment - 1] = replace_compartment
        compartment_list[value - 1] = self
        self.brain.compartments = tuple(compartment_list)
        assert self.brain[value] == self
        assert self.brain[old_id] == replace_compartment

    def __getitem__(self, index):
        if index < 0:
            raise IndexError("indices less than 0 not supported")
        elif index >= len(self.slices):
            self[index] = TracedSlice(self)
            return self.slices[index]
        return self.slices[index]

    def __delitem__(self, index):
        traced_slice = self.slices[index]
        for mesh in self.brain.meshes.values():
            if traced_slice in mesh:
                mesh.discard(traced_slice)
        del self.slices[index]

    def __setitem__(self, index, value):
        if not isinstance(value, TracedSlice):
            value = TracedSlice(self, *value)
        if value.compartment is not self:
            raise ValueError("TracedSlice does not have the correct brain_panel")
        if index > 300:
            raise IndexError("Number of slices in a compartment can not exceed 300")
        while len(self.slices) <= index:
            self.slices.append(TracedSlice(self))
        self.slices[index] = value

    def __iter__(self, ):
        for slice in self.slices:
            yield slice

    def insert(self, idx, value):
        if not isinstance(value, TracedSlice):
            value = TracedSlice(self, *value)
        if value.compartment is not self:
            raise ValueError("TracedSlice does not have the correct brain_panel")
        while len(self.slices) < idx:
            self.slices.append(TracedSlice(self))
        self.slices.insert(idx, value)

    def move(self, idx_old, idx_new):
        """
        Moves a TracedSlice to a new position in the Compartment

        :param idx_old: old index
        :param idx_new: new index
        """
        if idx_new == idx_old:
            return
        old_val = self[idx_old]
        step = 1 if idx_new > idx_old else -1
        for idx in range(idx_old, idx_new, step):
            self[idx] = self[idx + step]
        self[idx_new] = old_val
        assert len(set(self)) == len(self), "duplicate slices found in Compartment %i after moving" % self.id_compartment

    def to_json_object(self, ):
        """
        Returns a set of basic objects that can be stored in a json file

        :return: dictionary containing all the outlines
        """
        res = {}
        res['slices'] = [slice.to_json_object() for slice in self]
        res['stainings'] = list(self.stainings)
        return res

    def from_json_object(self, dict_obj):
        self.stainings = StainingList(dict_obj['stainings'])
        self.slices = [TracedSlice.from_json_object(self, item) for item in dict_obj['slices']]


class TracedSlice(MutableSequence):
    """
    Collection of outlines traced on a slice
    """
    _stainings = None

    def __init__(self, compartment, outlines=None, filename=None, register=None):
        """
        Defines a new stained slice as part of the given compartment

        :param compartment: the collection of slices of which this slice is a part
        :type compartment: Compartment
        """
        self.compartment = compartment
        self.outlines = []
        self.full_filename = filename
        if outlines is not None:
            self.extend(outlines)
        self.register = register

    @property
    def stainings(self, ):
        if self.compartment.stainings is None:
            return self._stainings
        return self.compartment.stainings

    @stainings.setter
    def stainings(self, value):
        if self.compartment.stainings is not None:
            raise ValueError("Stainings are defined at the compartment level")
        self._stainings = value

    def __len__(self, ):
        return len(self.outlines)

    def __getitem__(self, item):
        return self.outlines[item]

    def __delitem__(self, item):
        del self.outlines[item]

    def __setitem__(self, item, value):
        if not isinstance(value, Outline):
            value = Outline(self, *value)
        if value.slice is not self:
            raise ValueError("Outline does not have the correct brain_panel")
        self.outlines[item] = value

    def insert(self, idx, value):
        if not isinstance(value, Outline):
            value = Outline(self, *value)
        if value.slice is not self:
            raise ValueError("Outline does not have the correct brain_panel")
        self.outlines.insert(idx, value)

    def get_label(self, label):
        return [outline for outline in self.outlines if outline.label == label]

    @property
    def filename(self, ):
        if self.full_filename is None:
            return ''
        return os.path.split(self.full_filename)[1]

    @property
    def in_compartment(self, ):
        return self.compartment.slices.index(self)

    @in_compartment.setter
    def in_compartment(self, new_val):
        self.compartment.move(self.in_compartment, new_val)

    @property
    def in_brain(self, ):
        return self.in_compartment * self.compartment.brain.ncompartments + self.compartment.id_compartment

    @property
    def z(self, ):
        return self.compartment.brain.dist_compartments * self.in_brain

    def __len__(self):
        return len(self.outlines)

    def __iter__(self, ):
        for outline in self.outlines:
            yield outline

    def from_filename(self, filename):
        """
        Replaces the outlines with those from an ASCII file produced by neuolucida

        :param filename: .ASC filename
        """
        from .io.marked_slice import read_marked_slice
        self.outlines = []
        ms = read_marked_slice(filename)
        self.full_filename = filename
        for name, outlines in ms.items():
            for outline in outlines:
                self.append((outline.points[:, :2], name, outline.color))
        self.guess_outline()

    def to_filename(self, filename):
        """
        Writes the outlines to an ASCII file similar to the one produced by neuolucida

        :param filename: .ASC filename
        """
        from .io.marked_slice import write_marked_slice
        mapping = {}
        for outline in self:
            if outline.label not in mapping:
                mapping[outline.label] = []
            mapping[outline.label].append(outline.to_surface_1d())
        write_marked_slice(filename, mapping)

    def to_json_object(self, ):
        """
        Returns a set of basic objects that can be stored in a json file

        :return: dictionary containing all the outlines
        """
        labels = {}
        for outline in self:
            if outline.label not in labels:
                labels[outline.label] = []
            labels[outline.label].append({'points': list(outline.points.flatten()),
                                          'color': outline.color})
        res = {'labels': labels,
               'full_filename': self.full_filename,
               'register': self.register}
        return res

    @classmethod
    def from_json_object(cls, parent, dict_obj):
        res = cls(parent, filename=dict_obj['full_filename'], register=dict_obj.get('register', None))
        for label, outlines in dict_obj['labels'].items():
            for outline_dict in outlines:
                res.append(Outline(res, sp.array(outline_dict['points']).reshape(-1, 2), label,
                                   outline_dict['color']))
        return res

    def guess_outline(self, only=None):
        """
        Guesses which of the outlines correspond to the WM/GM boundary and Pial surface
        """
        if only not in (None, 'Pial surface', 'WM/GM boundary'):
            raise ValueError('No algorithm to guess outline for %s' % only)
        poss_outlines = [outline for outline in self if outline.points.shape[0] >= 50]

        def normed_quant(outline):
            resampled = utils.resample_contour(outline.points, 300)
            curv = utils.curvature(resampled)
            return curv / sp.std(curv), sp.sqrt(((resampled[1:] - resampled[:-1]) ** 2).sum(-1)).sum()

        if len(poss_outlines) > 0:
            c_outlines, l_outlines = zip(*[normed_quant(outline) for outline in poss_outlines])

            skew = sp.array([stats.skew(c) for c in c_outlines])
            mean = sp.array([sp.mean(c) for c in c_outlines])
            if only != 'Pial surface':
                usable = (skew < 0) & (skew > -2) & (mean > -0.3)
                idx_white = sp.argmax(l_outlines * usable)
                if sp.isfinite(mean[idx_white]):
                    self.compartment.brain.meshes['WM/GM boundary'].add(poss_outlines[idx_white])

            if only != 'WM/GM boundary':
                usable = (skew > 0)
                idx_pial = sp.argmax(l_outlines * usable)
                if skew[idx_pial] > 0:
                    self.compartment.brain.meshes['Pial surface'].add(poss_outlines[idx_pial])


class Outline(object):
    def __init__(self, slice, points, label, color):
        """
        Outline drawn on a slice

        :param slice: Slice on which the outline was drawn
        :type slice: TracedSlice
        :param points: (N, 2) array with the outline
        :param label: initial name of the outline
        :param color: default color of the outline
        """
        self.slice = slice
        self.points = utils.outward_normal(points)
        if points.ndim != 2 or points.shape[1] != 2:
            raise ValueError("Points should be an (N, 2) array")
        self.label = label
        self.color = color

    @property
    def stainings(self):
        return self.slice.stainings

    @property
    def idx_label(self, ):
        idx = 0
        for outline in self.slice.outlines:
            if outline is self:
                return idx
            if outline.label == self.label:
                idx += 1
        raise ValueError('Did not find this outline in brain_panel slice')

    @property
    def in_meshes(self, ):
        res = set()
        for mesh in self.slice.compartment.brain.meshes.values():
            if self in mesh.outlines:
                res.add(mesh)
        return frozenset(res)

    def registered_points(self, ):
        if self.slice.register is None:
            return self.points
        offset, angle = self.slice.register
        ct, st = sp.cos(angle), sp.sin(angle)
        x = ct * self.points[:, 0] - st * self.points[:, 1] + offset[0]
        y = st * self.points[:, 0] + ct * self.points[:, 1] + offset[1]
        return sp.stack((x, y), -1)

    def to_surface_1d(self, ):
        """
        Converts the outline to a Surface1D, which can be written to a Neuolucida file

        :return: outline in writable format
        """
        from .io.marked_slice import Surface1D
        points = sp.zeros((self.points.shape[0], 4))
        points[:, :2] = self.registered_points()
        points[:, 2] = self.slice.z
        points[:, 3] = 1.
        return Surface1D(points, self.label, self.color, closed=utils.is_closed(self.points))


class StainingList(Set):
    """
    Collection of stainings applied to the compartment/traced slice
    """
    possible_stains = {'AA', 'FS', 'FR', 'LY'}

    def __init__(self, stains):
        self.stains = set()
        for stain in stains:
            self.add(stain)

    def add(self, stain):
        """
        Adds a new stain to the StainingList

        :param stain: name of the stain
        """
        if stain == 'None':
            return
        if stain not in self.possible_stains:
            raise ValueError("Stain %s not recognized" % stain)
        self.stains.add(stain)

    def remove(self, stain):
        """
        Removes a stain from the StainingList

        :param stain: name of the stain
        """
        self.stains.remove(stain)

    def __len__(self):
        return len(self.stains)

    def __contains__(self, item):
        return item in self.stains

    def __iter__(self):
        return iter(self.stains)

    def __str__(self):
        return str(self.stains)

    def __repr__(self):
        return "StainingList(%s)" % self.stains


class Mesh(MutableSet):
    """
    A mesh connecting multiple outlines
    """
    def __init__(self, brain, register=True, align=True):
        """
        Creates a new mesh

        :param brain: monkey brain where the mesh is defined
        :type brain: Brain
        :param register: True if mesh should be used for registration
        :param align: True if mesh should be used for alignment
        """
        self.brain = brain
        self.outlines = set()
        self.register = register
        self.align = align

    @property
    def name(self, ):
        for name, mesh in self.brain.meshes.items():
            if self is mesh:
                return name
        raise ValueError("Mesh not found in brain")

    @name.setter
    def name(self, value):
        if value == self.name:
            return
        if value in self.brain.meshes:
            raise ValueError("Mesh with name %s already exists" % name)
        self.brain.meshes[value] = self

    def add(self, outline):
        for alt_outline in outline.slice:
            if alt_outline in self.outlines:
                self.outlines.remove(alt_outline)
        self.outlines.add(outline)

    def __contains__(self, item):
        """
        Returns True if the outline is part of the mesh (or the TracedSlice contains an outline that is part of the mesh)

        :param item: outline or collection of outlines in TracedSlice
        :type item: (TracedSlice, Outline)
        """
        if isinstance(item, Outline):
            return item in self.outlines
        elif isinstance(item, TracedSlice):
            for outline in self:
                if outline.slice is item:
                    return True
            return False
        raise TypeError("Item of type %s can not be part of a mesh" % type(item))

    def from_slice(self, slice):
        """
        Finds the outline in the slice corresponding to this mesh

        :param slice: collection of outlines
        :type slice: TracedSlice
        :return: mesh outline in the TracedSlice
        :rtype: Outline
        """
        for outline in slice:
            if outline in self:
                return outline
        raise IndexError("No outlines for %s contained in %s" % (self, slice))

    def __iter__(self, ):
        for outline in self.outlines:
            yield outline

    def __len__(self, ):
        return len(self.outlines)

    def discard(self, value):
        if isinstance(value, Outline):
            self.outlines.discard(value)
        elif isinstance(value, TracedSlice):
            for outline in set(self):
                if outline.slice is value:
                    self.discard(outline)
        else:
            raise TypeError("Item of type %s can not be part of a mesh" % type(value))

    def to_json_object(self, ):
        """
        Returns a set of basic objects that can be stored in a json file

        :return: dictionary containing all the outlines
        """
        dict_obj = {}
        dict_obj['outlines'] = [(outline.slice.compartment.id_compartment, outline.slice.in_compartment,
                                 outline.label, outline.idx_label) for outline in self.outlines]
        dict_obj['register'] = self.register
        dict_obj['align'] = self.align
        return dict_obj

    @classmethod
    def from_json_object(cls, parent, dict_obj):
        res = cls(parent, register=dict_obj.get('register', True), align=dict_obj.get('align', True))
        for id_comp, id_slice, label, id_label in dict_obj['outlines']:
            outline = res.brain[id_comp][id_slice].get_label(label)[id_label]
            res.add(outline)
        return res
