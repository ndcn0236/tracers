import matplotlib.pyplot as plt
from matplotlib.widgets import RectangleSelector
import scipy as sp


def select_roi(image):
    """
    Plots an image and waits for the user to select an ROI in the image.
    """
    fig = plt.figure()
    fig.clear()
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1)
    ax = fig.add_subplot(111)
    ax.imshow(image)
    box = ax.plot(sp.zeros(5), sp.zeros(5), 'k--')[0]

    def set_box(x1, y1, x2, y2):
        set_box.rs.data[()] = x1, y1, x2, y2
        box.set_data(sp.array([x1, x2, x2, x1, x1]),
                     sp.array([y1, y1, y2, y2, y1]))

    def move_box_selector(eclick, erelease):
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        set_box(x1, y1, x2, y2)
    ax.axis('off')

    set_box.rs = RectangleSelector(ax, move_box_selector,
                                   drawtype='box', useblit=True,
                                   button=[1, 3],  # don't use middle button
                                   minspanx=5, minspany=5,
                                   spancoords='pixels',
                                   interactive=True)
    set_box.rs.data = sp.zeros(4)
    set_box(-0.5, -0.5, image.shape[1] - 0.5, image.shape[0] - 0.5)
    plt.show(block=True)
    return set_box.rs.data
