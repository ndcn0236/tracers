Creates meshes of brain structures from slices with traced outlines

# Installation
Start a terminal and run the following commands
```bash
cd /path/to/Download
cd tracers
python setup.py install
```
This will install the `tracers` python library including the script to start the GUI under the name `tracers`.
During the installation any required python libraries not yet present will also be installed.

If the installation of these python libraries fails it might be helpful to install the following python libraries in an alternative method before running the commands above:
- [numpy](http://www.numpy.org/)
- [scipy](https://scipy.org/)
- [matplotlib](http://matplotlib.org/)
- [wx](https://wiki.wxpython.org/ProjectPhoenix)

The easiest way to install these libraries is through [anaconda](https://www.continuum.io/downloads) or [pip](https://docs.python.org/3/installing/index.html)

# Aligning the brain slices

## Starting the GUI
Run `tracers` in the command line. This produces a GUI that (after loading some data) looks like:

![Screenshot of the GUI](GUI_image.jpg)
## Loading new slices/compartments
The outlines traced in Neurolucida can be loaded into the GUI in 2 ways:
- A new compartment can be loaded by selecting "New compartment" in the file menu. Simply select *all* the files containing the traced slices for a single comparmtent. This will produce a new column in the GUI with a default compartment number. Change this compartment number to the right value and select the appropriate stains and counterstains.
- When a compartment already has been loaded, a single slice can be replaced by selecting the slice to be replaced and pressing "r" (or selecting "Replaces slice" in the Slice menu).

NOTICE: you might have to resize the window after loading the data to actually see the new data!

## Sorting the slices within a compartment
When loading a new compartment the GUI will try to sort them automatically based on the filenames. If the resulting slices are out of order, this can be adjusted by selecting the offending slices (i.e., by clicking on them) and dragging them to a new location (or using the arrow keys to move them up/down).

Empty slices can be inserted at any point by selecting the current image and pressing "e" (or selecting "insert empty slice" from the Slice menu).

## Correcting the mesh selections
The pial surface (i.e., brain outline) and WM/GM boundary meshes are available by default (TODO: allow users to add meshes). There is a decent automated algorithm to select the WM/GM boundary and pial surface by default, however you will probably have to correct the selection for some of the slices. The current mesh can be selected on the upper left. The outlines corresponding to this mesh are plotted in bold. To correct any mistakes of the algorithm first make sure that the checkbox next to "Enables selecting of outlines" is checked. Then simply click the correct outline, which should cause that outline to become bold. If the outline corresponding to the current mesh was not traced in a specific slice just click on the bold outline to deselect it.

Don't forget to deselect the "Enables selecting of outlines" checkbox before continuing to stop accidental selecting of outlines.

Make sure that both the pial surface and WM/GM boundary outlines are correctly selected in every slice before continuing to the next steps!

## Aligning the compartments with each other
In this step we use the pial surface and WM/GM boundary to correctly align compartments with each other. The simplest way to do this is to select "Align compartments" in the "Process" menu. To adjust any remaining errors in the alignment you can move individual compartments up or down by selecting any slice within that compartment and using the arrow keys while keeping the shift-key down (note that the arrow keys move individual slices within a compartment if the shift key is not pressed).

## Rigid body registration
Initial rough version available by selecting "Rigid body registration" in the "Process" menu. This will use all the WM/GM bondaries and pial surfaces to estimate the alignment.

I am still working on a more robust version

## Storing the current outline
At any point the current progress can be saved in a json file by selecting "Save" or "Save as" in the "File" menu. It would be good practice to have a single json file for every monkey, which will contain all the outlines traced for that monkey.

The outlines registered to each other can be saved into ASCII files using "Export" in the "File" menu. Select a directory where each slice will be stored under a filename like:
MN166_S010_C003_FR (the slice shown in the 10th row for compartment 3 of monkey MN 166, which was stained with FR)

Future versions will hopefully allow export to an imod file directly.
