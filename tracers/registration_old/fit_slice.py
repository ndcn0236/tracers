from .cost import MeshContour
import scipy as sp
from scipy import optimize


def affine_matrix(rotation=0., sx=1., sy=1., shear=0.):
    """
    Gets the affine matrix with the given rotation, shear, and scaling

    :param rotation: rotation in radians
    :param sx: scaling in the x-dimension
    :param sy: scaling in the y-dimansion
    :param shear: amount of shear in radians
    :return: (2, 2) array to be multiplied with the (x, y) old coordinates to get the new coordinates
    """
    return sp.array([[sx * sp.cos(rotation), sy * sp.sin(rotation + shear)],
                     [-sx * sp.sin(rotation), sy * sp.cos(rotation + shear)]])


def transform(points, xshift=0., yshift=0., rotation=0., sx=1., sy=1., shear=0.):
    """Transforms the points using an affine transform

    :param points: (N, 2) array with the initial positions
    :param xshift: shift in the x-direction
    :param yshift: shift in the y-direction
    :param rotation: rotation in radians
    :param sx: scaling in the x-dimension
    :param sy: scaling in the y-dimansion
    :param shear: amount of shear in radians
    :return: (N, 2) array with the final positions
    """
    offset = sp.array([xshift, yshift])
    mat = affine_matrix(rotation, sx, sy, shear)
    return sp.dot(points, mat.T) + offset


class SliceComparison(object):
    def __init__(self, imod, surface, zval):
        """
        Register the contours in the marked slices to the corresponding meshes in imod

        :param imod: an object from an imod file
        :param surface: contour as traced in neuolocida
        :param zval: z-value of the surface
        """
        self.imod = imod
        self.surface = surface
        self.imod_contour = MeshContour(self.imod.slice(zval))

        offset = sp.sqrt(sp.sum((self.surface.points[1:, :2] - self.surface.points[:-1, :2]) ** 2, -1))
        self.weight = sp.zeros(offset.shape[0] + 1)
        self.weight[:-1] += offset
        self.weight[1:] += offset
        self.weight /= 2

        self.surface_center = (self.surface.points[:, :2] * self.weight[:, None]).sum(0) / self.weight.sum()

    def transformed(self, params):
        """
        Returns the transformed points

        :param params: 2-6 parameters describing in order (x-offset, y-offset, rotation, x-scaling, y-scaling, shear)
        :return: (N, 2) array with the x- and y-coordinates after transformation
        """
        points = self.surface.points[:, :2] - self.surface_center
        return transform(points, *params)

    def cost(self, params):
        """
        Returns the cost function of the offset

        :param params: 2-6 parameters describing in order (x-offset, y-offset, rotation, x-scaling, y-scaling, shear)
        :return: cost function
        """
        if len(params) < 2 or len(params) > 6 or len(params) == 4:
            raise ValueError('Invalid number of parameters')
        dist = self.imod_contour.dist_points(self.transformed(params))
        return (dist * self.weight).sum()

    def single_fit(self, params, method='Nelder-Mead', **kwargs):
        """Fits the given (number of) parameters in the slice

        :param params: initialization parameters
        :param method: name of the optimization method to use
        :return:
        """
        if isinstance(params, int):
            params = self.def_params()[:params]
        return optimize.minimize(self.cost, params, method=method, **kwargs)

    def def_params(self, ):
        scaling = self.imod_contour.control_points.std(0) / self.surface.points[:, :2].std(0)
        offset = self.imod_contour.control_points.mean(0)
        return offset[0], offset[1], 0., scaling[0], scaling[1], 0.

    def plot(self, params, axes=None):
        """
        Plots the surfaces after transformation on the axes

        :param params: 2-6 parameters describing in order (x-offset, y-offset, rotation, x-scaling, y-scaling, shear)
        :param axes:
        :return:
        """
        from matplotlib import collections
        if axes is None:
            from matplotlib.pyplot import gca
            axes = gca()
        points = self.transformed(params)
        axes.plot(points[:, 0], points[:, 1], '-', color='C0')
        lc = collections.LineCollection(self.imod_contour.line_segments, color='C1')
        axes.add_collection(lc)
        return axes
