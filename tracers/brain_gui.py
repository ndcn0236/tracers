import wx
from . import brain, utils
import matplotlib as mpl
mpl.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
import os
import scipy as sp
from matplotlib.collections import LineCollection


class BrainFrame(wx.Frame):
    """
    Frame illustrating all the outlines drawn for a single brain
    """
    _slice_selected = None
    _current_mesh = None

    def __init__(self, brain, parent=None, title=None, filename=None, **kwargs):
        """
        Creates a panel based on the traced brain

        :param brain: traced monkey brain
        :type brain: brain.Brain
        :param kwargs: keyword arguments for the panel setup
        """
        if title is None:
            title = "Tracing for monkey %s" % brain.identifier
        super(BrainFrame, self).__init__(parent, title=title, **kwargs)
        self.brain = brain
        self.filename = filename

        self.CreateStatusBar()

        filemenu = wx.Menu()
        newitem = filemenu.Append(wx.ID_NEW, "&New brain")
        self.Bind(wx.EVT_MENU, self.load, newitem)
        additem = filemenu.Append(wx.ID_OPEN, "New &compartment")
        self.Bind(wx.EVT_MENU, self.new_compartment, additem)
        save_item = filemenu.Append(wx.ID_SAVE, "&Save")
        self.Bind(wx.EVT_MENU, self.save, save_item)
        save_as_item = filemenu.Append(wx.ID_SAVEAS, "&Save As")
        self.Bind(wx.EVT_MENU, self.save_as, save_as_item)
        export_item = filemenu.Append(wx.ID_ANY, "Export")
        self.Bind(wx.EVT_MENU, self.export, export_item)

        viewmenu = wx.Menu()
        self.view_filename = viewmenu.AppendCheckItem(wx.ID_ANY, "Show filenames")
        self.view_filename.Check()
        self.view_alignment = viewmenu.AppendCheckItem(wx.ID_ANY, "View alignment")
        self.Bind(wx.EVT_MENU, self.toggle_view_alignment, self.view_alignment)
        self.view_register = viewmenu.AppendCheckItem(wx.ID_ANY, "View registration")
        self.view_register.Check()
        self.Bind(wx.EVT_MENU, self.toggle_view_register, self.view_register)

        processmenu = wx.Menu()
        guess_pial = processmenu.Append(wx.ID_ANY, 'Guess Pial surface')
        self.Bind(wx.EVT_MENU, self.guess_pial, guess_pial)
        guess_white = processmenu.Append(wx.ID_ANY, 'Guess WM/GM boundary')
        self.Bind(wx.EVT_MENU, self.guess_white, guess_white)
        align_compartments = processmenu.Append(wx.ID_ANY, 'Align compartments')
        self.Bind(wx.EVT_MENU, self.align_compartments, align_compartments)
        register_mesh = processmenu.Append(wx.ID_ANY, 'Rigid-body registration')
        self.Bind(wx.EVT_MENU, self.register, register_mesh)

        compartment_menu = wx.Menu()
        additem = compartment_menu.Append(wx.ID_OPEN, "New &compartment")
        self.Bind(wx.EVT_MENU, self.new_compartment, additem)
        compartment_move_up = compartment_menu.Append(wx.ID_ANY, 'Move compartment up')
        self.Bind(wx.EVT_MENU, self.compartment_move_up, compartment_move_up)
        compartment_move_down = compartment_menu.Append(wx.ID_ANY, 'Move compartment down')
        self.Bind(wx.EVT_MENU, self.compartment_move_down, compartment_move_down)
        compartment_remove = compartment_menu.Append(wx.ID_ANY, 'Delete compartment')
        self.Bind(wx.EVT_MENU, self.compartment_remove, compartment_remove)

        slice_menu = wx.Menu()
        slice_move_up = slice_menu.Append(wx.ID_ANY, 'Move slice up')
        self.Bind(wx.EVT_MENU, self.slice_move_up, slice_move_up)
        slice_move_down = slice_menu.Append(wx.ID_ANY, 'Move slice down')
        self.Bind(wx.EVT_MENU, self.slice_move_down, slice_move_down)
        flip_slice = slice_menu.Append(wx.ID_ANY, 'Flips the slice')
        self.Bind(wx.EVT_MENU, self.flip_slice, flip_slice)
        remove_slice = slice_menu.Append(wx.ID_ANY, 'Delete slice')
        self.Bind(wx.EVT_MENU, self.remove_slice, remove_slice)
        insert_empty = slice_menu.Append(wx.ID_ANY, 'Insert empty slice')
        self.Bind(wx.EVT_MENU, self.insert_empty, insert_empty)
        replace_slice = slice_menu.Append(wx.ID_ANY, 'Replaces slice')
        self.Bind(wx.EVT_MENU, self.replace_slice, replace_slice)

        mesh_menu = wx.Menu()
        label_mesh = mesh_menu.Append(wx.ID_ANY, 'New mesh')
        self.Bind(wx.EVT_MENU, self.new_mesh_from_label, label_mesh)
        delete_mesh = mesh_menu.Append(wx.ID_ANY, 'Delete mesh')
        self.Bind(wx.EVT_MENU, self.delete_mesh, delete_mesh)

        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")
        menuBar.Append(viewmenu, "&View")
        menuBar.Append(processmenu, "&Process")
        menuBar.Append(compartment_menu, "&Compartment")
        menuBar.Append(slice_menu, "&Slice")
        menuBar.Append(mesh_menu, "&Mesh")
        self.SetMenuBar(menuBar)

        self.setup_panel()
        self.SetSize((400, 400))
        self.Layout()

    def setup_panel(self, ):
        self.mesh_ctrl = wx.ComboBox(self, style=wx.TE_PROCESS_ENTER, choices=list(self.brain.meshes.keys()))
        self.mesh_ctrl.Bind(wx.EVT_COMBOBOX, self.change_mesh)
        self.mesh_ctrl.Bind(wx.EVT_TEXT_ENTER, self.change_mesh)

        self.align_mesh = wx.CheckBox(self, label='Use mesh in alignment')
        self.align_mesh.Bind(wx.EVT_CHECKBOX, self.change_align)
        self.register_mesh = wx.CheckBox(self, label='Use mesh in registration')
        self.register_mesh.Bind(wx.EVT_CHECKBOX, self.change_register)

        if len(self.mesh_ctrl.GetValue()) != 0:
            self.current_mesh = self.brain.meshes[self.mesh_ctrl.GetValue()]

        self.all_compartment_panels = AllCompartmentsPanel(self)

        # setting the option panel
        self.species_ctrl = wx.ComboBox(self, choices=list(self.brain.possible_species),
                                        style=wx.CB_READONLY, value=self.brain.species)
        self.species_ctrl.Bind(wx.EVT_COMBOBOX, self.change_species)

        self.name_ctrl = wx.TextCtrl(self)
        self.name_ctrl.SetLabelText(self.brain.identifier)
        self.name_ctrl.Bind(wx.EVT_TEXT, self.change_name)

        brain_option_inner = wx.BoxSizer(wx.HORIZONTAL)
        brain_option_inner.Add(self.species_ctrl)
        brain_option_inner.Add(self.name_ctrl)

        option_outer = wx.BoxSizer(wx.VERTICAL)
        option_outer.Add(wx.StaticText(self, label='Monkey settings'))
        option_outer.Add(brain_option_inner)
        option_outer.Add(wx.StaticText(self, label='Mesh settings'))
        option_outer.Add(self.mesh_ctrl)
        option_outer.Add(self.align_mesh)
        option_outer.Add(self.register_mesh)
        option_outer.Add(wx.StaticText(self, label="""
WARNING: make sure you regularly save 
using "File menu > Save"!

Steps to register brain outlines:
1. Load any traced compartment by selecting 
the ASCII files in "File menu > New compartment"
2. Manually sort the slices in a compartment 
using drag-and-drop or by selecting one and 
using the arrow keys
3. Select "WM/GM boundaries" and "Pial surface" 
in the "Mesh settings" dropbox above and 
select the correct outline in each slice using
shift + click (selected outline will be bold)
4. Run "Process > Align Compartments"
5. Manually adjust alignment using the options
in "Compartments menu" or shift + arrow key
6. Run "Process > Registration"
7. Look for errors in the registration; these
are typically caused by
 - slices being out of order (step 2)
 - wrong outline identified as pial 
   or WM/GM boundary (step 3)
 - compartments being out of order (step 5)
8. Export to ASCII file (imod not yet available)"""))

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(option_outer)
        sizer.Add(self.all_compartment_panels)
        self.SetSizer(sizer)
        self.SetAutoLayout(1)
        sizer.Fit(self)
        self.Show()

    def change_name(self, evt):
        """
        Change the monkey name
        """
        self.brain.identifier = self.name_ctrl.GetLineText(0)
        self.SetTitle("Tracing for monkey %s" % self.brain.identifier)

    def change_species(self, evt):
        """
        Change the species of the monkey
        """
        self.brain.species = self.species_ctrl.GetValue()

    def new_compartment(self, evt):
        """
        Opens the data for a compartment
        """
        dlg = wx.FileDialog(self, "Choose all files for new compartment", '.', "", "*.ASC", wx.FD_OPEN + wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            directory = dlg.GetDirectory()
            filenames = [os.path.join(directory, filename) for filename in dlg.GetFilenames()]
            for compartment in self.brain.empty_compartments():
                for idx, filename in enumerate(utils.sort_filenames(filenames)):
                    compartment[idx].from_filename(filename)
                self.all_compartment_panels.update()
                break

    def load(self, evt):
        dlg = wx.FileDialog(self, "Open a brain file", '.', "", "*.json", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            filename = os.path.join(dlg.GetDirectory(), dlg.GetFilename())
            if os.path.isfile(filename):
                initial = brain.Brain.load(filename)
            else:
                initial = brain.Brain('empty', 'MN')
            BrainFrame(initial, filename=self.filename)

    def save(self, evt):
        if self.filename is None:
            self.save_as(evt)
        else:
            self.brain.save(self.filename)

    def save_as(self, evt):
        dlg = wx.FileDialog(self, "Choose a brain file", '.', "", "*.json", wx.FD_SAVE + wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = os.path.join(dlg.GetDirectory(), dlg.GetFilename())
            self.brain.save(self.filename)

    def export(self, evt):
        dlg = wx.DirDialog(self, "Choose directory to store the output", '.', wx.DD_DIR_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            self.brain.export(dlg.GetPath())

    def replot(self, evt=None, full=False):
        self.all_compartment_panels.replot(full=full)

    def change_mesh(self, evt=None):
        name = self.mesh_ctrl.GetValue()
        if self.current_mesh is not None and self.current_mesh.name == name:
            return
        if len(name) == 0:
            self.current_mesh = None
            return
        if name not in self.brain.meshes:
            if self.current_mesh is None:
                return
            old_name = self.current_mesh.name
            self.brain.meshes[name] = self.current_mesh
            del self.brain.meshes[old_name]
            names = list(self.brain.meshes.keys())
            self.mesh_ctrl.Clear()
            self.mesh_ctrl.AppendItems(names)
            self.mesh_ctrl.SetValue(name)
        else:
            self.current_mesh = self.brain.meshes[name]
            self.align_mesh.SetValue(self.current_mesh.align)
            self.register_mesh.SetValue(self.current_mesh.register)
            self.replot()

    def toggle_view_filename(self, evt=None):
        for compartment in self.all_compartment_panels.plot_panels.values():
            for slice in compartment.slice_panels.values():
                if self.view_filename.IsChecked():
                    slice.title.Show()
                else:
                    slice.title.Hide()
        self.Layout()

    def guess_pial(self, evt=None):
        for compartment in self.brain:
            for slice in compartment:
                slice.guess_outline(only='Pial surface')
        self.replot()

    def guess_white(self, evt=None):
        for compartment in self.brain:
            for slice in compartment:
                slice.guess_outline(only='WM/GM boundary')
        self.replot()

    def align_compartments(self, evt=None):
        self.brain.align_compartments()
        self.all_compartment_panels.update()

    @property
    def slice_selected(self):
        return self._slice_selected

    @slice_selected.setter
    def slice_selected(self, new_value):
        """

        :type new_value: SlicePanel
        :return:
        """
        if new_value is self._slice_selected:
            return
        self._slice_selected = new_value

    @property
    def current_mesh(self, ):
        return self._current_mesh

    @current_mesh.setter
    def current_mesh(self, new_val):
        if new_val is self.current_mesh:
            return
        self._current_mesh = new_val
        if new_val is None:
            self.align_mesh.SetValue(False)
            self.register_mesh.SetValue(False)
        else:
            self.align_mesh.SetValue(new_val.align)
            self.register_mesh.SetValue(new_val.register)

    def compartment_move_up(self, evt=None):
        """
        Moves the selected compartment up one
        """
        if self.slice_selected is not None:
            self.slice_selected.compartment_panel.move_up()

    def compartment_move_down(self, evt=None):
        """
        Moves the selected compartment down one
        """
        if self.slice_selected is not None:
            self.slice_selected.compartment_panel.move_down()

    def compartment_remove(self, evt=None):
        """
        Removes the selected compartment
        """
        if self.slice_selected is not None:
            self.slice_selected.compartment_panel.remove()

    def slice_move_up(self, evt=None):
        """
        Moves the selected slice up one
        """
        if self.slice_selected is not None:
            self.slice_selected.move_to(self.slice_selected.slice.in_compartment - 1)

    def slice_move_down(self, evt=None):
        """
        Moves the selected slice down one
        """
        if self.slice_selected is not None:
            self.slice_selected.move_to(self.slice_selected.slice.in_compartment + 1)

    def flip_slice(self, evt=None):
        """
        Flips the selected slice in the x-dimension
        """
        if self.slice_selected is not None:
            self.slice_selected.flip(dim=0)

    def remove_slice(self, evt=None):
        """
        Removes the selected slice
        """
        if self.slice_selected is not None:
            self.slice_selected.remove_slice()

    def insert_empty(self, evt=None):
        """
        Inserts an empty slice at the selected position
        """
        if self.slice_selected is not None:
            self.slice_selected.insert_empty()

    def replace_slice(self, evt=None):
        """
        Inserts an empty slice at the selected position
        """
        if self.slice_selected is not None:
            self.slice_selected.replace()

    def update(self, evt=None):
        """
        Refreshes the outline plots
        """
        self.all_compartment_panels.update()

    def change_align(self, evt=None):
        if self.current_mesh is not None:
            self.current_mesh.align = self.align_mesh.IsChecked()

    def change_register(self, evt=None):
        if self.current_mesh is not None:
            self.current_mesh.register = self.register_mesh.IsChecked()

    def toggle_view_alignment(self, evt=None):
        self.all_compartment_panels.replot()
        self.Layout()

    def toggle_view_register(self, evt=None):
        self.all_compartment_panels.replot(full=True)
        self.Layout()

    def register(self, evt=None):
        self.brain.register()
        if self.view_register.IsChecked():
            self.all_compartment_panels.replot(full=True)

    def delete_mesh(self, evt=None):
        if self.current_mesh is None:
            return
        name = self.current_mesh.name
        self.mesh_ctrl.Delete(self.mesh_ctrl.GetSelection())
        del self.brain.meshes[name]
        self.mesh_ctrl.SetSelection(wx.NOT_FOUND)
        self.current_mesh = None
        self.replot(full=False)

    def new_mesh_from_label(self, evt=None):
        """
        Creates a new mesh based on a neurolicida label
        """
        labels = set()
        for slice in self.brain.slices():
            for outline in slice:
                labels.add(outline.label)
        label = wx.GetSingleChoice("Choose a neurolucida label to populate a new mesh. The longest outline with that label in every slice will be included in the new mesh.",
                                   'New mesh from label', ['Empty mesh'] + sorted(labels))
        mesh_name = label
        while mesh_name in self.brain.meshes:
            mesh_name += '+'
        self.brain.meshes[mesh_name] = brain.Mesh(self.brain, register=False, align=False)
        self.mesh_ctrl.Append(mesh_name)
        self.mesh_ctrl.SetValue(mesh_name)
        self.current_mesh = self.brain.meshes[mesh_name]
        if label != 'Empty mesh':
            for slice in self.brain.slices():
                valid = [outline for outline in slice if outline.label == label]
                if len(valid) > 0:
                    lengths = [utils.length(outline.points) for outline in valid]
                    idx = sp.argmax(lengths)
                    self.current_mesh.add(valid[idx])
        self.all_compartment_panels.replot()


class AllCompartmentsPanel(wx.Panel):
    """
    Panel containing the outlines for all the compartments
    """

    def __init__(self, parent):
        """
        :type parent: BrainFrame
        """
        super(AllCompartmentsPanel, self).__init__(parent)
        self.brain_panel = parent
        self.scrollable = wx.ScrolledWindow(self, )

        self.scrollable.SetScrollRate(10, 10)
        self.plot_panels = {comp: CompartmentPanel(self, comp) for comp in parent.brain.compartments}
        self.setting_panels = {comp: CompartmentInfoPanel(self, comp) for comp in parent.brain.compartments}
        self.shown_compartments = []
        self.Bind(wx.EVT_SIZE, self.on_size)

        self.update()

    def update(self, ):
        sizer = wx.BoxSizer(wx.VERTICAL)
        upper_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.lower_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.shown_compartments = []
        for comp in self.GetParent().brain.compartments:
            if not comp.empty():
                panel = self.plot_panels[comp]
                self.shown_compartments.append(comp)
                panel.update()
                self.lower_sizer.Add(panel)
                panel.Show()

                upper_sizer.Add(self.setting_panels[comp])
                self.setting_panels[comp].Show()
        sizer.Add(upper_sizer)
        sizer.Add(self.scrollable)

        self.scrollable.SetSizer(self.lower_sizer)
        self.scrollable.SetAutoLayout(1)
        self.lower_sizer.FitInside(self.scrollable)
        self.scrollable.Show()

        self.SetSizer(sizer)
        self.SetAutoLayout(1)
        sizer.FitInside(self)
        self.Show()
        self.adjust_scrollbar()
        self.Layout()

    def replot(self, full=False):
        for comp_panel in self.plot_panels.values():
            for slice_panel in comp_panel.slice_panels.values():
                if full:
                    slice_panel.plot_outlines()
                else:
                    slice_panel.update_outlines()
        self.brain_panel.Layout()

    def __iter__(self, ):
        for comp in self.shown_compartments:
            yield self.plot_panels[comp]

    def __getitem__(self, index):
        return self.plot_panels[self.shown_compartments[index]]

    def on_size(self, evt=None):
        self.adjust_scrollbar()
        self.Layout()

    def adjust_scrollbar(self, ):
        """
        Sets up the scrollbar for total image of given width and height
        """
        fws, fhs = self.GetClientSize()
        wt, ht = self.lower_sizer.GetMinSize()
        self.scrollable.SetMinSize((fws, fhs))
        self.scrollable.SetScrollbar(wx.VERTICAL, 0., fhs, ht, True)
        self.scrollable.SetScrollbar(wx.HORIZONTAL, 0., fws, wt, True)
        self.Layout()


class CompartmentPanel(wx.Panel):
    """
    Column of traced slices
    """

    def __init__(self, all_compartments, compartment):
        """

        :param all_compartments: panel containing all compartments
        :type all_compartments: AllCompartmentsPanel
        :param compartment: single stained compartment with zero or more traced outlines
        :type compartment: brain.Compartment
        """
        super(CompartmentPanel, self).__init__(all_compartments.scrollable, style=wx.RAISED_BORDER, size=(200, -1))
        self.compartment = compartment
        self.all_compartments = all_compartments

        # setting the plot panel
        self.slice_panels = {}

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)
        self.SetAutoLayout(1)
        sizer.Fit(self)
        self.Hide()

    def update(self, ):
        sizer = wx.BoxSizer(wx.VERTICAL)
        old_slices = list(self.slice_panels.keys())
        for slice in self.compartment.slices:
            if slice not in self.slice_panels:
                self.slice_panels[slice] = SlicePanel(self, slice)
            else:
                old_slices.remove(slice)
            sizer.Add(self.slice_panels[slice])
        for slice in old_slices:
            del self.slice_panels[slice]
        self.option_panel.update()
        self.SetSizer(sizer)
        self.SetAutoLayout(1)
        sizer.Fit(self)
        self.Show()
        self.Layout()

    def __getitem__(self, idx):
        if idx >= len(self.compartment):
            raise IndexError("Slice panel with index %i has not been created yet" % idx)
        return self.slice_panels[self.compartment[idx]]

    def move_down(self, skip_update=False):
        """
        Moves all the slices one step down
        """
        self.compartment.insert(0, ())
        if not skip_update:
            self.brain_panel.update()

    def move_up(self, ):
        """
        Moves all the slices one step up
        """
        if len(self.compartment[0]) == 0:
            del self.compartment[0]
        else:
            for comp_panel in self.parent.compartment_panels:
                if comp_panel is not self:
                    comp_panel.move_down(skip_update=True)
        self.brain_panel.update()

    def remove(self, ):
        """
        Removes the given compartment from the brain
        """
        while len(self.compartment.slices) != 0:
            del self.compartment[0]
        self.compartment.stainings.stains = {}
        self.brain_panel.update()

    def __iter__(self):
        for slice in self.compartment.slices:
            yield self.slice_panels[slice]

    @property
    def option_panel(self, ):
        return self.all_compartments.setting_panels[self.compartment]

    @property
    def brain_panel(self, ):
        return self.all_compartments.brain_panel


class CompartmentInfoPanel(wx.Panel):
    def __init__(self, all_compartments, compartment):
        """

        :param all_compartments: Panel containing all the compartment columns
        :type all_compartments: AllCompartmentsPanel
        :param compartment: specific compartment to show information about
        """
        super(CompartmentInfoPanel, self).__init__(all_compartments, size=(200, -1))
        self.all_compartments = all_compartments
        self.compartment = compartment
        choices = ([str(compartment.id_compartment)] +
                   [str(comp.id_compartment) for comp in compartment.brain.empty_compartments()
                    if comp != compartment])

        self.id_comp_ctrl = wx.ComboBox(self, choices=choices,
                                        style=wx.CB_READONLY)
        self.id_comp_ctrl.Bind(wx.EVT_COMBOBOX, self.change_id)
        self.stain_ctrls = [wx.ComboBox(self, choices=['None'] + list(compartment.stainings.possible_stains),
                                        style=wx.CB_READONLY) for _ in range(3)]
        for ctrl in self.stain_ctrls:
            ctrl.Bind(wx.EVT_COMBOBOX, self.change_stain)

        full_sizer = wx.BoxSizer(wx.VERTICAL)
        sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer1.Add(self.id_comp_ctrl)
        sizer1.Add(self.stain_ctrls[0])
        sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer2.Add(self.stain_ctrls[1])
        sizer2.Add(self.stain_ctrls[2])
        full_sizer.Add(sizer1)
        full_sizer.Add(sizer2)
        self.SetSizer(full_sizer)
        self.SetAutoLayout(1)
        self.update()
        full_sizer.Fit(self)
        self.Hide()

    @property
    def plot_panel(self, ):
        return self.all_compartments.plot_panels[self.compartment]

    @property
    def brain_panel(self, ):
        return self.all_compartments.brain_panel

    def update(self, ):
        """
        Updates the ID & stain values based on the current settings
        """
        choices = ([str(self.compartment.id_compartment)] +
                   [str(comp.id_compartment) for comp in self.compartment.brain.empty_compartments()
                    if comp != self.compartment])
        self.id_comp_ctrl.Clear()
        self.id_comp_ctrl.AppendItems(choices)
        self.id_comp_ctrl.SetValue(str(self.compartment.id_compartment))
        stains = list(self.compartment.stainings)
        while len(stains) < 3:
            stains.append('None')
        for ctrl, stain in zip(self.stain_ctrls, stains):
            ctrl.SetValue(stain)

    def change_id(self, evt=None):
        """
        Updates the id of the compartment
        """
        if self.compartment.id_compartment == int(self.id_comp_ctrl.GetValue()):
            return
        self.compartment.id_compartment = int(self.id_comp_ctrl.GetValue())
        self.brain_panel.update()

    def change_stain(self, evt=None):
        """
        Updates the stains selected for a compartment
        """
        stains = [ctrl.GetValue() for ctrl in self.stain_ctrls]
        self.compartment.stainings = brain.StainingList(stains)
        if self.compartment.empty():
            self.brain_panel.update()


class CompartmentInfoDialog(wx.Dialog):
    def __init__(self, parent, filenames):
        super(CompartmentInfoDialog, self).__init__(parent)
        self.panel = CompartmentInfoPanel(self, parent.brain.empty_compartments()[0])
        self.parent = parent
        self.filenames = filenames

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(self.panel)
        vbox.Add(hbox2)

        self.SetSizer(vbox)
        self.SetAutoLayout(1)
        vbox.Fit(self)

        okButton.Bind(wx.EVT_BUTTON, self.ok_button)
        closeButton.Bind(wx.EVT_BUTTON, self.close_button)

    def ok_button(self, event):
        compartment = self.parent.brain[int(self.panel.id_comp_ctrl.GetValue())]
        compartment.stainings = brain.StainingList([cntrl.GetValue() for cntrl in self.panel.stain_ctrls])
        for idx, filename in enumerate(utils.sort_filenames(self.filenames)):
            compartment[idx].from_filename(filename)
        self.Close()

    def close_button(self, event):
        self.Close()


class SlicePanel(wx.Panel):
    def __init__(self, compartment_panel, slice):
        """
        Creates a new panel illustrating a single slice

        :param compartment_panel: Panel containing all slices of a single compartment
        :type compartment_panel: CompartmentPanel
        :param slice: Tracings on a single traced slice
        :type slice: brain.TracedSlice
        """
        self.figure = Figure()
        self.compartment_panel = compartment_panel
        super(SlicePanel, self).__init__(compartment_panel, size=(190, 190))
        self.canvas = FigureCanvasWxAgg(self, wx.ID_ANY, self.figure)
        self.slice = slice

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.title = wx.StaticText(self, label='no filename' if len(self.slice.filename) == 0 else self.slice.filename)
        sizer.Add(self.title)
        sizer.Add(self.canvas)
        if self.brain_panel.view_filename.IsChecked():
            self.title.Show()
        else:
            self.title.Hide()
        self.lines = None
        self.mesh_line = None
        self.alignment_lines = None

        self.plot_outlines()

        for part in (self, self.canvas, self.title):
            part.Bind(wx.EVT_KEY_DOWN, self.key_press)
            part.Bind(wx.EVT_LEFT_UP, self.end_drag)
        self.canvas.mpl_connect('pick_event', self.outline_picker)

        self.SetSizer(sizer)
        self.SetAutoLayout(1)
        sizer.Fit(self)
        self.Show()

    def plot_outlines(self, ):
        """
        Replots the outlines

        Call if:
        - outline shape has been changed (e.g. due to registration)
        """
        if self.brain_panel.view_filename.IsChecked():
            self.title.Show()
        else:
            self.title.Hide()
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.figure.subplots_adjust(top=1, bottom=0, right=1, left=0)
        self.axes.set_xticks([])
        self.axes.set_yticks([])
        self.lines = []
        self.mesh_line = None
        self.alignment_lines = None
        for outline in self.slice.outlines:
            if outline.points.shape[0] > 3:
                p = outline.registered_points() if self.brain_panel.view_register.IsChecked() else outline.points

                color = utils.to_mpl_color(outline.color)
                line = self.axes.plot(p[:, 0], p[:, 1], '-', color=color,
                                      picker=0 if outline.points.shape[0] <= 3 else 5)[0]
                line.outline = outline
                self.lines.append(line)

        self.update_outlines()

    def update_outlines(self, ):
        """
        Updates how previously drawn outlines are plotted

        Adjusts for:
        - which outline is the current mesh
        - which slice is currently selected
        - whether the alignment with the selected slice is shown
        """
        show_alignment = (self.brain_panel.current_mesh is not None and
                          self.brain_panel.view_alignment.IsChecked() and
                          not self.selected and
                          self.brain_panel.slice_selected is not None and
                          self is not self.brain_panel.slice_selected and
                          (abs(self.slice.in_brain - self.brain_panel.slice_selected.slice.in_brain) <=
                           self.brain_panel.brain.ncompartments) and
                          self.slice in self.brain_panel.current_mesh and
                          self.brain_panel.slice_selected.slice in self.brain_panel.current_mesh)
        mesh = self.brain_panel.current_mesh
        if mesh is None or show_alignment:
            if self.mesh_line is not None:
                self.mesh_line.set_linewidth(1)
                self.mesh_line = None
        elif self.mesh_line is None or self.mesh_line.outline not in mesh:
            if self.mesh_line is not None:
                self.mesh_line.set_linewidth(1)
                self.mesh_line = None
            for line in self.lines:
                if line.outline in mesh:
                    self.mesh_line = line
                    self.mesh_line.set_linewidth(3)
                    break

        self.axes.set_facecolor('gray' if self.selected else 'white')

        if self.alignment_lines is not None:
            for line in self.alignment_lines:
                line.remove()
            self.alignment_lines = None
        if show_alignment:
            from . import registration
            mesh = self.brain_panel.current_mesh
            selected = self.brain_panel.slice_selected.slice
            outline_selected = mesh.from_slice(selected)
            outline = mesh.from_slice(self.slice)

            self.alignment_lines = []
            points_selected = (outline_selected.registered_points() if self.brain_panel.view_register.IsChecked() else
                               outline_selected.points)
            points = (outline.registered_points() if self.brain_panel.view_register.IsChecked() else
                      outline.points)

            color = utils.to_mpl_color(outline_selected.color)
            self.alignment_lines.extend(self.axes.plot(points_selected[:, 0],
                                                       points_selected[:, 1], color=color,
                                                       linestyle='--', linewidth=1))
            springs = sp.array(registration.spring_outlines(points, points_selected, 500))

            lc = LineCollection(springs.transpose((1, 0, 2)), linestyles='-', color=color,
                                linewidths=1, cmap='jet', array=sp.linspace(0, 1, springs.shape[1]))
            self.alignment_lines.append(lc)
            self.axes.add_collection(lc)

    def key_press(self, evt=None):
        if self.slice not in self.compartment_panel.compartment.slices or not self.selected:
            return
        kc = evt.GetKeyCode()
        if kc == wx.WXK_DOWN:
            if evt.ShiftDown():
                self.compartment_panel.move_down()
            else:
                self.move_to(self.slice.in_compartment + 1)
        elif kc == wx.WXK_UP:
            if evt.ShiftDown():
                self.compartment_panel.move_up()
            else:
                self.move_to(self.slice.in_compartment - 1)
        elif kc in [wx.WXK_DELETE, wx.WXK_BACK]:
            self.remove_slice()
        elif chr(kc).lower() == 'f':
            self.flip()
        elif chr(kc).lower() == 'e':
            self.insert_empty()
        elif chr(kc).lower() == 'r':
            self.replace()
        else:
            evt.Skip()

    def move_to(self, index):
        """
        Moves the slice to a new position

        :param index: new index of the slice
        """
        if index == -1 or index == len(self.slice.compartment) or self.slice.in_compartment == index:
            return
        self.slice.in_compartment = index
        self.compartment_panel.update()
        self.compartment_panel.all_compartments.replot()
        self.brain_panel.Layout()

    def flip(self, dim=0):
        """
        Flips the slice in the chosen dimension

        :param dim: 0 for x-direction; 1 for y-direction
        """
        for outline in self.slice:
            outline.points[:, dim] *= -1
            outline.points = outline.points[::-1, :]
        self.plot_outlines()
        self.compartment_panel.all_compartments.replot()
        self.compartment_panel.Layout()

    def end_drag(self, evt):
        if self.slice not in self.compartment_panel.compartment.slices:
            return
        evt.Skip()
        pos = evt.GetPosition()
        if self.HitTest(pos) == wx.HT_WINDOW_OUTSIDE:
            screen_pos = self.ClientToScreen(pos)
            for slice_panel in self.compartment_panel:
                slice_pos = slice_panel.ScreenToClient(screen_pos)
                if slice_panel.HitTest(slice_pos) != wx.HT_WINDOW_OUTSIDE:
                    self.move_to(slice_panel.slice.in_compartment)
                    return
        else:
            self.selected = True
        self.compartment_panel.all_compartments.replot()

    def remove_slice(self, ):
        idx = self.slice.in_compartment
        del self.compartment_panel.compartment[self.slice.in_compartment]
        if len(self.compartment_panel.compartment) >= idx:
            idx -= 1
        if idx < 0:
            idx += 1
        if len(self.compartment_panel.compartment) == 0:
            self.brain_panel.update()
        else:
            self.compartment_panel[idx].selected = True
            self.compartment_panel[idx].SetFocus()
            self.compartment_panel.update()
            self.brain_panel.replot(full=False)

    def outline_picker(self, evt):
        mesh = self.brain_panel.current_mesh
        if wx.GetKeyState(wx.WXK_SHIFT) and isinstance(evt.artist, Line2D) and mesh is not None:
            if mesh is not None:
                if evt.artist.outline in mesh:
                    mesh.remove(evt.artist.outline)
                else:
                    mesh.add(evt.artist.outline)
                self.update_outlines()
                self.compartment_panel.Layout()

    @property
    def selected(self, ):
        return self.brain_panel.slice_selected is self

    @selected.setter
    def selected(self, new_value):
        if new_value:
            self.brain_panel.slice_selected = self
        else:
            if self.selected:
                self.brain_panel.slice_selected = None

    @property
    def brain_panel(self):
        return self.compartment_panel.brain_panel

    def insert_empty(self, evt=None):
        """
        Inserts an empty slice at the current location
        """
        self.slice.compartment.insert(self.slice.in_compartment, ())
        self.compartment_panel.update()

    def replace(self, filename=None, evt=None):
        if filename is None:
            dlg = wx.FileDialog(self, "Choose a file to replace %s" % self.slice.filename,
                                '.', "", "*.ASC", wx.FD_OPEN + wx.FD_MULTIPLE)
            if dlg.ShowModal() != wx.ID_OK:
                return
            filename = os.path.join(dlg.GetDirectory(), dlg.GetFilename())
        self.slice.from_filename(filename)
        self.title.SetLabelText(self.slice.filename)
        self.plot_outlines()
        self.compartment_panel.Layout()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("Starts a GUI to register traced outlines")
    parser.add_argument('brain', nargs='?', help='.json file with the initial filename')
    args = parser.parse_args()

    if args.brain is not None and os.path.isfile(args.brain):
        initial = brain.Brain.load(args.brain)
    else:
        initial = brain.Brain('default_name', 'MN')
    app = wx.App(False)
    frame = BrainFrame(initial, filename=args.brain)
    app.MainLoop()
