"""
Registers contours to each other
"""
import scipy as sp
from .. import utils


class NormalMatcher(object):
    """
    Matches a traced outline to the surface using the normal
    """
    _outlines = {}

    def __init__(self, reference, moving, double=True):
        """
        Fits the moving to the reference outline
        """
        self.reference = reference
        self.ref_norm = utils.normal(reference)
        self.moving = moving
        self.double = double

    def npoints(self, ):
        if self.double:
            return (self.reference.shape[0] + 1) // 2
        else:
            return self.reference.shape[0]

    def correlate(self, down_scale):
        """
        Correlates the moving outline normals to the reference normals

        :param down_scale: Which fraction of the reference outline is matched by the moving outline
        :return: tuple with two cross correlations (one with the outline flipped)
        """
        cont = utils.resample(self.moving, int(self.npoints * down_scale))
        nm_mov = utils.normal(cont)
        c1 = sp.sum([sp.correlate(nm_mov[:, dim], self.ref_norm[:, dim], 'valid') for dim in range(2)], 0) / (2 * self.npoints)
        c2 = sp.sum([sp.correlate(-nm_mov[::-1, dim], self.ref_norm[:, dim], 'valid') for dim in range(2)], 0) / (2 * self.npoints)
        return c1, c2

    def match_normal(self, try_downscale=300):
        """
        Finds where the outline matches the mesh at the given z-values

        :param try_zval: sequence of z-values to cut the mesh
        :param npoints: number of points to sample the mesh
        :return: tuple with (maximum correlation, z-value of outline, start of outline, end of outline)
        """
        if isinstance(try_downscale, int):
            try_downscale = sp.logspace(-1.5, 0, try_downscale)
        max_corr = -sp.inf
        xmin = 0
        xmax = 1
        for down_scale in try_downscale:
            c1, c2 = self.correlate(down_scale)
            if c1.max() > max_corr:
                max_corr = c1.max()
                xmin = 1. - c1.argmax() / self.npoints
                xmax = xmin + down_scale
            if c2.max() > max_corr:
                max_corr = c2.max()
                xmin = c2.argmax() / self.npoints + 1
                xmax = xmin - down_scale
        return max_corr, xmin, xmax


    def cost_curvature(self, params, smooth, return_cont=False):
        """
        Computes the cost of the curvature fit

        :param params:
        :param smooth: how important is the smoothing
        :param return_cont:
        :return:
        """
        params = sp.asarray(params)
        nbins = len(params) - 1
        if (((params[1:] - params[:-1]) > 0).sum() not in (0, nbins) or
                (params[1:] == params[:-1]).any() or
                    params[0] < 0 or params[-1] > 2):
            return sp.inf

        along_surf = sp.zeros(cont.shape[0])
        xbins = sp.linspace(0, nbins, cont.shape[0])
        idx_bin = sp.floor(xbins).astype('int')
        idx_bin[-1] -= 1
        xval = (params[idx_bin + 1] * (xbins - idx_bin) +
                params[idx_bin] * (idx_bin + 1 - xbins))
        if return_cont:
            return ref_interp(xval)
        curv_ref = curv_interp(xval[1:-1])
        norm = sp.mean(abs(curv_ref)) / sp.mean(abs(curv))

        steepness = params[1:] - params[:-1]
        penalty = smooth * sp.sum((steepness[1:] - steepness[:-1]) ** 2)
        # print(penalty, stats.pearsonr(curv_ref, curv)[0])
        return penalty - stats.pearsonr(curv_ref, curv)[0]

