#!/usr/bin/env python

from distutils.core import setup

setup(name='tracers',
      version='0.0',
      description='Utility functions to analyze tracer data',
      author='Michiel Cottaar',
      author_email='Michiel.Cottaar@ndcn.ox.ac.uk',
      url='https://git.fmrib.ox.ac.uk/ndcn0236/tracers',
      packages=['tracers', 'tracers.io', 'tracers.gui', 'tracers.registration_old'],
      install_requires=["numpy", "scipy", "matplotlib", "wxPython"]
     )