from .alignment import align_outlines
import scipy as sp
from . import utils
from.brain import Brain
from scipy import sparse, optimize


def spring_outlines(outline1, outline2, dist):
    """
    Returns regularly spaced springs between two outlines

    :param outline1: (N, 2) array with traced points
    :param outline2: (M, 2) array with traced points
    :param dist: typical distance between two springs
    :return: 2 (K, 2) arrays the K points which should overlap
    """
    ((s1, r1), (s2, r2)), _ = align_outlines(outline1, outline2)
    if r1 < s1:
        r1 += utils.length(outline1)
    if r2 < s2:
        r2 += utils.length(outline2)
    d1 = r1 - s1
    d2 = r2 - s2

    dmean = (d1 + d2) / 2.
    npoints = int(sp.around(dmean / dist))

    extend1 = utils.is_closed(outline1) and d1 == utils.length(outline1)
    dpoints1 = sp.linspace(s1, r1, npoints + 1 + extend1)
    if extend1:
        dpoints1 = dpoints1[:-1]
    extend2 = utils.is_closed(outline2) and d2 == utils.length(outline2)
    dpoints2 = sp.linspace(s2, r2, npoints + 1 + extend2)
    if extend2:
        dpoints2 = dpoints2[:-1]
    return utils.interpolate(outline1, dpoints1), utils.interpolate(outline2, dpoints2)


def affine_springs(spring1, spring2):
    """
    Returns the matrix used (3, 2) affine matrices for both springs to the Euclidean offset

    For (3, 2) affine matrices A1 and A2 and resulting matrix M:
    Cost = \sum_{ijklm} M_{limj} Al_{ik} Am_{jk}

    :param spring1: (K, 2) array with points that should overlap with `spring2`
    :param spring2: (K, 2) array with points that should overlap with `spring1`
    :return: (2, 3, 2, 3) array that maps the 2 affine transforms to the cost function
    """
    def to_affine(points1, points2):
        res = sp.zeros((3, 3))
        res[:2, :2] = sp.dot(points1.T, points2)
        res[-1, :2] = points2.sum(0)
        res[:2, -1] = points1.sum(0)
        res[-1, -1] = points1.shape[0]
        return res

    res = sp.zeros((2, 3, 2, 3))
    res[0, :, 0, :] = to_affine(spring1, spring1)
    res[0, :, 1, :] = -to_affine(spring1, spring2)
    res[1, :, 0, :] = -to_affine(spring2, spring1)
    res[1, :, 1, :] = to_affine(spring2, spring2)
    return res


def rigid_to_affine(offset, theta=0):
    """
    Computes the affine transformations corresponding to a given offset/angle or rotation

    :param offset: (N, 2) array with offsets for N outlines
    :param theta: (N, ) array with angles of rotation
    :return: (N, 3, 2) array with affine transformations
    """
    if offset.ndim == 1:
        N = 1
        offset = offset[None, :]
    else:
        N = offset.shape[0]
    res = sp.zeros((N, 3, 2))
    res[:, -1, :] = offset
    ct, st = sp.cos(theta), sp.sin(theta)
    res[:, 0, 0] = ct
    res[:, 1, 1] = ct
    res[:, 0, 1] = st
    res[:, 1, 0] = -st
    return res


class RegistrationCost(object):
    def __init__(self, brain, dstep=100.):
        """
        Prepares the computation of the registration cost function for the brain

        :param brain:
        :type brain: Brain
        :param dstep: typical distance between two springs
        """
        self.brain = brain
        self.connections = []
        self.nslices = 0
        for idx1, slice1 in enumerate(self.brain.slices()):
            self.nslices += 1
            for idx2, slice2 in enumerate(self.brain.slices()):
                if abs(slice2.in_brain - slice1.in_brain) <= self.brain.ncompartments and idx2 > idx1:
                    for mesh in self.brain.meshes.values():
                        res = None
                        if slice1 in mesh and slice2 in mesh:
                            outline1 = mesh.from_slice(slice1)
                            outline2 = mesh.from_slice(slice2)
                            spring1, spring2 = spring_outlines(outline1.points, outline2.points, dstep)
                            aff = affine_springs(spring1, spring2)
                            res = aff if res is None else res + aff
                        if res is not None:
                            self.connections.append((idx1, idx2, spring1.shape[0], aff,
                                                     spring1.sum(0) - spring2.sum(0)))

    def _offset_mat_calc(self, ):
        """
        Returns the (M * 2, M * 2) sparse matrix that maps from the (M, 2) offsets to the cost function
        """
        row = sp.zeros(len(self.connections) * 12, dtype='i4')
        col = sp.zeros(len(self.connections) * 12, dtype='i4')
        value = sp.zeros(len(self.connections) * 12)
        for idx_conn, (idx1, idx2, aff) in enumerate(self.connections):
            idx_mat = idx_conn * 12
            row[idx_mat : idx_mat + 8] = idx1 * 2
            row[idx_mat + 8 : idx_mat + 12] = idx2 * 2

            col[idx_mat : idx_mat + 4] = idx1 * 2
            col[idx_mat + 4 : idx_mat + 12] = idx2 * 2

            value[idx_mat : idx_mat + 4] = aff[0, -1, :, 0, -1, :].flatten()
            value[idx_mat + 4 : idx_mat + 8] = (aff[0, -1, :, 1, -1, :] + aff[1, -1, :, 0, -1, :]).flatten()
            value[idx_mat + 8 : idx_mat + 12] = aff[1, -1, :, 1, -1, :].flatten()
        row[1::2] += 1
        col[2::4] += 1
        col[3::4] += 1

        return sparse.coo_matrix((value, (row, col)), shape=(self.nslices * 2, self.nslices * 2))

    def _affine_mat_calc(self, ):
        """
        Returns the (M * 6, M * 6) sparse matrix that maps from the (M, 6) affine transformation parameters to the cost function
        """
        row = sp.zeros(len(self.connections) * 108, dtype='i4')
        col = sp.zeros(len(self.connections) * 108, dtype='i4')
        value = sp.zeros(len(self.connections) * 108)
        for idx_conn, (idx1, idx2, aff) in enumerate(self.connections):
            idx_mat = idx_conn * 108
            row[idx_mat : idx_mat + 72] = idx1 * 6
            row[idx_mat + 72 : idx_mat + 108] = idx2 * 6

            col[idx_mat : idx_mat + 36] = idx1 * 6
            col[idx_mat + 36 : idx_mat + 108] = idx2 * 6

            value[idx_mat : idx_mat + 36] = aff[0, :, :, 0, :, :].flatten()
            value[idx_mat + 36 : idx_mat + 72] = (aff[0, :, :, 1, :, :] + aff[1, :, :, 0, :, :]).flatten()
            value[idx_mat + 72 : idx_mat + 108] = aff[1, :, :, 1, :, :].flatten()
        for i in range(6):
            row[i::6] += i
            for j in range(6):
                col[i + j * 6::36] += j

        return sparse.coo_matrix((value, (row, col)), shape=(self.nslices * 6, self.nslices * 6))

    def offset_cost(self, offset, derivative=False):
        """
        Computes the cost for a given offset

        :param offset: (M, 2) array
        :return: float with the Euclidean offset
        """
        if offset.shape != (self.nslices, 2):
            raise ValueError("Offset does not have expected shape of (%i, 2)" % self.nslices)
        if derivative:
            der = sp.zeros(offset.shape)
            for idx1, idx2, npoints, _, sum_offset in self.connections:
                add_der = 2 * (npoints * (offset[idx1] - offset[idx2]) + sum_offset)
                der[idx1] += add_der
                der[idx2] -= add_der
            return der
        else:
            cost = 0.
            for idx1, idx2, npoints, _, sum_offset in self.connections:
                cost += (2 * (sum_offset * (offset[idx1] - offset[idx2])).sum() +
                         npoints * ((offset[idx1] - offset[idx2]) ** 2).sum())
            return cost

    def rigid_cost(self, offset, angle, derivative=False):
        """
        Computes the cost for a given rigid transformation

        :param offset: (M, 2) array
        :param angle: (M, ) array
        :return: float with the Euclidean offset
        """
        if offset.shape != (self.nslices, 2):
            raise ValueError("Offset does not have expected shape of (%i, 2)" % self.nslices)
        if angle.shape != (self.nslices, ):
            raise ValueError("Angle does not have expected shape of (%i, )" % self.nslices)
        affines = sp.zeros((offset.shape[0], 3, 2))
        affines[:, -1, :] = offset
        ct, st = sp.cos(angle), sp.sin(angle)
        affines[:, 0, 0] = ct
        affines[:, 1, 1] = ct
        affines[:, 0, 1] = st
        affines[:, 1, 0] = -st
        res = self.affine_cost(affines, derivative=derivative)
        if derivative:
            offset_der = res[:, -1, :]
            angle_der = (-(res[:, 0, 0] + res[:, 1, 1]) * st +
                         (res[:, 0, 1] - res[:, 1, 0]) * ct)
            return offset_der, angle_der
        else:
            return res

    def affine_cost(self, affines, derivative=False):
        """
        Computes the cost given affine transformations for every slice.

        :param affines: (M, 3, 2) array
        :return:
        """
        if derivative:
            der = sp.zeros(affines.shape)
            for idx1, idx2, _, aff, _ in self.connections:
                actual_affine = affines[[idx1, idx2]]
                der[[idx1, idx2]] += sp.tensordot(aff, actual_affine, ([0, 1], [0, 1]))
                der[[idx1, idx2]] += sp.tensordot(aff, actual_affine, ([2, 3], [0, 1]))
            return der
        else:
            cost = 0.
            for idx1, idx2, _, aff, _ in self.connections:
                actual_affine = affines[[idx1, idx2]]
                actual_affine_squared = sp.tensordot(actual_affine, actual_affine, (-1, -1))
                cost += (aff * actual_affine_squared).sum()
            return cost

    def initial_offset(self, ):
        """
        Uses the median position of every slice to compute the initial offset

        :return: (M, 2) offset array
        """
        res = sp.zeros((self.nslices, 2))
        for idx, slice in enumerate(self.brain.slices()):
            all_points = sp.concatenate([outline.points for outline in slice], 0)
            res[idx] = -sp.median(all_points, 0)
        return res

    def best_offset(self, starting=None):
        """
        Computes the best-fit offset

        :param starting: (M, 2) offset array (defaults to initial_offset)
        :return: (M, 2) best-fit offset array
        """
        if starting is None:
            starting = self.initial_offset()
        f = lambda p: self.offset_cost(p.reshape((self.nslices, 2)), derivative=False)
        df = lambda p: self.offset_cost(p.reshape((self.nslices, 2)), derivative=True).flatten()
        res = optimize.fmin_bfgs(f, starting.flatten(), df)
        return res.reshape(starting.shape)

    def best_rigid(self, start_offset=None, start_angle=None):
        """
        Computes the best-fit rigid transformation

        :param start_offset: (M, 2) array with the initial offset (defaults to best_offset)
        :param start_angle: (M, ) array with the initial angles (defaults to zeros)
        :return: (M, 2) array of offsets and (M, ) array of angles
        """
        if start_offset is None:
            start_offset = self.best_offset()
        if start_angle is None:
            start_angle = sp.zeros(self.nslices)
        starting = sp.concatenate((start_offset, start_angle[:, None]), -1)

        def f(p):
            p_shaped = p.reshape((self.nslices, 3))
            return self.rigid_cost(p_shaped[:, :2], p_shaped[:, -1], derivative=False)

        def df(p):
            p_shaped = p.reshape((self.nslices, 3))
            der_offset, der_angle = self.rigid_cost(p_shaped[:, :2], p_shaped[:, -1], derivative=True)
            return sp.concatenate((der_offset, der_angle[:, None]), -1).flatten()
        res = optimize.fmin_bfgs(f, starting, df)
        res_shaped = res.reshape((self.nslices, 3))
        return res_shaped[:, :2], res_shaped[:, -1]

