import re
import scipy as sp
from .. import utils


class Surface1D(object):
    """Closed or open 1D surface as manually traced on a slice
    """
    def __init__(self, points, name, color=None, guid=None, mbf_object_type=None,
                 fill_density=None, resolution=None, closed=None):
        """
        New closed or open trace

        :param points: (N, 4) array with the x, y, z, resolution of the points
        :param name: name of the trace
        :param color: color name
        :param guid: unique identifier
        :param mbf_object_type: some number
        :param fill_density: some number
        :param resolution:
        :param closed: True if the surface is closed
        """
        self.points = sp.asarray(points)
        self.name = name
        self.color = color
        self.guid = guid
        self.mbf_object_type = mbf_object_type
        self.fill_density = fill_density
        self.resolution = resolution
        self.closed = closed
        if not utils.is_closed(self.points[:, :2]) and self.closed:
            self.points = sp.append(self.points, self.points[0, None, :], 0)

    @classmethod
    def from_text(cls, text):
        name = re.search(r'^\("(.+)"', text).group(1)
        kwargs = {}
        for keyword, identifier in [('color', 'Color'), ('guid', 'GUID'),
                                    ('mbf_object_type', 'MBFObjectType'),
                                    ('fill_density', 'FillDensity'),
                                    ('resolution', 'Resolution')]:
            found = re.search(r'\(%s (.+)\)' % identifier, text)
            if found is not None:
                kwargs[keyword] = found.group(1)
        for keyword in ('mbf_object_type', 'fill_density', 'resolution'):
            if keyword in kwargs:
                kwargs[keyword] = float(kwargs[keyword])
        closed = re.search('\(Closed\)', text) is not None
        arr = []
        for match in re.finditer(r'\(([\-0-9. ]+)\)', text):
            number_text = match.group(1).strip()
            arr.append([float(number) for number in number_text.split()])
        return cls(arr, name, closed=closed, **kwargs)

    def to_text(self, contour_id=1):
        options = []
        for keyword, identifier in [
                ('color', 'Color'), ('guid', 'GUID'),
                ('mbf_object_type', 'MBFObjectType'),
                ('fill_density', 'FillDensity'),
                ('resolution', 'Resolution')]:
            options.append('(%s %s)' % (identifier, getattr(self, keyword)))
        options = '\n  '.join(options)

        coordinates = []
        for idx, (x, y, z, res) in enumerate(self.points):
            coordinates.append('(%.2f %.2f %.2f %.2f)  ;%i, %i' % (x, y, z, res, contour_id, idx))
        coordinates = '\n  '.join(coordinates)

        name = self.name
        return """("%(name)s"
  %(options)s
  %(coordinates)s
)  ; End of countour""" % locals()


def read_marked_slice(filename):
    """
    Reads a ASCII file with the manual traces on a slice

    :param filename: name of file (normally ends with .ASC)
    :return: dictionary mapping surface names to list of Surface1D objects
    """
    surfaces = []
    with open(filename, 'r') as f:
        in_surface = False
        for line in f.readlines():
            if not in_surface:
                if re.search(r'^\("(.+)"', line) is not None:
                    text = line
                    in_surface = True
            else:
                if 'End of contour' in line:
                    surfaces.append(Surface1D.from_text(text))
                    in_surface = False
                else:
                    text += line
    if in_surface:
        raise ValueError('Last surface was not closed properly')
    res = {}
    for surf in surfaces:
        if surf.name not in res:
            res[surf.name] = []
        res[surf.name].append(surf)
    return res


def write_marked_slice(filename, mapping):
    """
    Writes a ASCII file with the manual traces on a slice

    :param filename: new filename (usually ends with .ASC)
    :param mapping: dictionary mapping surface names to list of Surface1D objects
    :return: string that has been written to the file
    """
    sorted_names = sorted(mapping.keys())
    texts = []
    idx = 1
    for name in sorted_names:
        for surface in mapping[name]:
            texts.append(surface.to_text(idx))
            idx += 1
    full_text = '\n\n'.join(texts)
    with open(filename, 'w') as f:
        f.write(full_text)
    return full_text