import numpy as np
from scipy import spatial
import numba


class MeshContour(object):
    """
    Describes a mesh as a set of connections between control points
    """
    def __init__(self, line_segments, dmax=1e-6):
        """
        Creates a new mesh from the given line segements

        :param line_segments: (N, 2, 2) array with line segments
        """
        self.line_segments = line_segments
        nsegments = line_segments.shape[0]
        tree = spatial.cKDTree(line_segments.reshape(nsegments * 2, 2))
        indices = tree.query_pairs(dmax, output_type='ndarray')
        use = np.ones(nsegments * 2, dtype='bool')
        use[indices[:, 1]] = False
        self.control_points = line_segments.reshape(nsegments * 2, 2)[use]
        self.control_tree = spatial.cKDTree(self.control_points)

        self.lines_to_points = -np.ones(nsegments * 2, dtype='i4')
        self.lines_to_points[use] = np.arange(use.sum())
        while (self.lines_to_points == -1).any():
            self.lines_to_points[indices[:, 1]] = self.lines_to_points[indices[:, 0]]

        self.points_to_lines = []
        assigned = np.append(self.lines_to_points.copy(), -1)
        while (assigned != -1).any():
            idx_points, idx_lines = np.unique(assigned, return_index=True)
            use = np.zeros(self.control_points.shape[0], dtype='bool')
            use[idx_points[1:]] = True
            self.points_to_lines.append((use, idx_lines[1:] // 2))
            assigned[idx_lines[1:]] = -1


    def dist_points(self, points):
        """Computes the distance from the points to the contour

        :param points: (M, 2) array with points of interest
        :return:
        """
        dist, indices = self.control_tree.query(points)
        for use, idx_lines in self.points_to_lines:
            possible = use[indices]
            line_segments = self.line_segments[idx_lines[indices[possible]]]
            points_poss = points[possible]
            line_offset = line_segments[:, 1, :] - line_segments[:, 0, :]
            denominator = np.sqrt((line_offset ** 2).sum(-1))
            nominator = abs(line_offset[:, 1] * points_poss[:, 0] -
                            line_offset[:, 0] * points_poss[:, 1] +
                            line_segments[:, 1, 0] * line_segments[:, 0, 1] -
                            line_segments[:, 1, 1] * line_segments[:, 0, 0])
            new_dist = nominator / denominator

            replace = new_dist < dist[possible]
            possible[possible] = replace
            dist[possible] = new_dist[replace]
        return dist

def contour_contour(contour_fixed, contour_moving, tree_fixed=None, weight=None):
    """
    Computes the offset between two contours

    :param contour_fixed:
    :param contour_moving:
    :param tree_fixed:
    :param weight:
    :return: moving contour length times average distance from fixed contour
    """
    dist = dist_points(contour_fixed, contour_moving, tree_fixed)
    if weight is None:
        offset = np.sqrt(((contour_moving[1:] - contour_moving[:-1]) ** 2).sum(-1))
        weight = np.zeros(contour_moving.shape[0])
        weight[:-1] += offset
        weight[1:] += offset
    return (weight * dist).sum() / 2


def dist_points(contour, points, tree_contour=None):
    """
    Computes the point-wise distance from every point to the contour

    :param contour: (2, N) array with the contour
    :param points: (2, M) array with points of interest
    :param tree_contour: pre-computed tree to efficiently compute distances to the contour control points
    :type tree_contour: spatial.cKDTree
    :return: (M, ) array with the distances from the point to the contour
    """
    if tree_contour is None:
        tree_contour = spatial.cKDTree(contour)
    dist, indices = tree_contour.query(points)
    closed = (contour[0] == contour[-1]).all()
    print(dist, indices)
    correct_dist(dist, indices, contour, points, closed)
    print(dist, indices)
    return dist


@numba.jit(nopython=True)
def correct_dist(dist, indices, contour_fixed, contour_moving, closed=False):
    """
    Checks the neighbouring line segments from the nearest control point to find a smaller distance

    Will adjust the `dist` array with the smaller distance if found

    :param dist: (M, ) array with the distances to the nearest control points on the fixed contour (will be altered in place) by this method)
    :param indices: (M, ) int array with the indices of the nearest control points on the fixed contour
    :param contour_fixed: (N, 2) array with the fixed contour
    :param contour_moving: (M, 2) array with the moving contour
    :param closed: True if the last fixed contour control point is the same as the first
    """
    for idx_moving in range(len(dist)):
        idx_fixed = indices[idx_moving]
        fpx = contour_fixed[idx_fixed, 0]
        fpy = contour_fixed[idx_fixed, 1]
        mpx = contour_moving[idx_moving, 0]
        mpy = contour_moving[idx_moving, 1]
        point_offsetx = mpx - fpx
        point_offsety = mpy - fpy
        for correction in (-1, 1):
            idx_other = idx_fixed + correction
            if idx_other == -1:
                if not closed:
                    continue
                idx_other = len(contour_fixed) - 2
            elif idx_other == len(contour_fixed):
                if not closed:
                    continue
                idx_other = 1
            opx = contour_fixed[idx_other, 0]
            opy = contour_fixed[idx_other, 1]
            line_offsetx = opx - fpx
            line_offsety = opy - fpy
            print(point_offsety, point_offsetx, line_offsety, line_offsetx)
            if ((point_offsetx * line_offsetx) + (point_offsety * line_offsety)) <= 0:
                continue
            denominator = np.sqrt(line_offsetx ** 2 + line_offsety ** 2)
            nominator = abs(line_offsety * mpx - line_offsetx * mpy +
                            opx * fpy - opy * fpx)
            new_dist = nominator / denominator
            if new_dist < dist[idx_moving]:
                dist[idx_moving] = new_dist
